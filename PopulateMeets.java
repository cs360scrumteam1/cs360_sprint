package model.database;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.StringTokenizer;
import model.meets.Regional;
import model.meets.School;
import model.meets.Sectional;
import model.meets.SemiState;
import model.meets.State;

public class PopulateMeets {
  private MeetData database;
  private ArrayList<School> schools;
  private ArrayList<Sectional> sectionals;
  private ArrayList<Regional> regionals;
  private ArrayList<SemiState> semistates;
  private ArrayList<State> states;
  private ArrayList<String> tempMeet;
  private int sectionalNum;
  private int regionalNum;
  private int semistateNum;
  private int stateNum;
  private BigDecimal tempLatitude;
  private BigDecimal tempLongitude;
  private final int compLevelOptions = 3;
  private String tempHost;
  private String tempName;
  private String  tempAddress;
  private String tempCompLevel; 
  private String tempStore;
  private float tempDistance;
  private float tempAverageDistance;
  private float tempMaxDistance;
  private boolean isHost;
  private StringTokenizer tokenizer;
  private final String delims = ";";
  private ExportMeets exm;


  // Temporarily hard coded values for number of unique meets.

  public PopulateMeets() {
    this.tempHost = "";
    this.database = new MeetData();

    // initializes the total meet numbers.
    sectionalNum = database.numSectionals();
    regionalNum = database.numRegionals();
    semistateNum = database.numSemiStates();
    stateNum = database.numStates();

    
    //Initializes the export class
    exm =  new ExportMeets(this);
    
    // Initializes all the Schools, and Meets.
    populateSectional();
    populateRegional();
    populateSemiState();
    populateStates();

  }

  // schoolname, address, competitionlevel, sectiondistance, latitude, longitude
  private void populateSectional() {
    this.sectionals = new ArrayList<Sectional>(); // initializes Sectional ArrayList

    for (int i = 0; i < this.sectionalNum; i++) {
      this.sectionals.add(new Sectional(i + 1)); // Adds a new Sectional to the Sectional ArrayList

      this.tempAverageDistance = database.loadSectionalAverage(i + 1); // Adds average distance to sectional
      sectionals.get(i).setAverageDistance(tempAverageDistance);

      this.tempMaxDistance = database.loadSectionalMax(i + 1); // Adds farthest distance to sectional
      sectionals.get(i).setFarthestDistance(tempMaxDistance);

      this.tempMeet = database.loadSectionalData(i + 1); // Loads ArrayList of Sectional data (School, Name, level, address)
      this.schools = new ArrayList<School>(); // initializes temporary ArrayList of Schools to add to a Meet
      this.tempHost = this.database.loadSectionalHosts(i + 1); // Loads the Sectional Host
      
      for (int a = 0; a < tempMeet.size(); a++) {
        this.isHost = false;

        tempStore = tempMeet.get(a); // Stores the string to be tokenized
        tokenizer = new StringTokenizer(tempStore, delims); // Tokenization of String with given delims
        tempName = tokenizer.nextToken(); // Gets the school name in the list of tokens to be added to the School object
        tempAddress = tokenizer.nextToken();// Gets the Address to be added to the School school
        tempCompLevel = tokenizer.nextToken(); // Gets the competitive level to be added to the
        tempDistance = Float.parseFloat(tokenizer.nextToken()); // Gets the distance to the host.
        tempLatitude = BigDecimal.valueOf(Double.parseDouble(tokenizer.nextToken())); //gets latitude of school
        tempLongitude = BigDecimal.valueOf(Double.parseDouble(tokenizer.nextToken())); // gets longitude of the school
        
        this.tempAverageDistance += tempDistance;

        if (tempName.equals(tempHost)) {
          this.isHost = true;
        }

        schools.add(new School(tempName, tempAddress, isHost, tempCompLevel, tempDistance, tempLatitude, tempLongitude));
      }
      this.sectionals.get(i).setParticipants(schools);
      this.sectionals.get(i).setHost(tempHost);
    }
  }

  private void populateRegional() {
    this.regionals = new ArrayList<Regional>(); // initializes ArrayList of Regional meets

    for (int i = 0; i < this.regionalNum; i++) {
      this.regionals.add(new Regional(i + 1)); // Initializes a new Regional

      this.tempAverageDistance = database.loadRegionalAverage(i + 1); // Adds average distance to sectional
      regionals.get(i).setAverageDistance(tempAverageDistance);

      this.tempMaxDistance = database.loadRegionalMax(i + 1); // Adds farthest distance to sectional
      regionals.get(i).setFarthestDistance(tempMaxDistance);

      this.tempMeet = database.loadRegionalData(i + 1); // retrieves the Regional Schools and stores them temporarily
      this.schools = new ArrayList<School>(); // temporary ArrayList of schools to assign to each Regional
      this.tempHost = this.database.loadRegionalHosts(i + 1);

      for (int a = 0; a < tempMeet.size(); a++) {
        this.isHost = false;
        tempStore = tempMeet.get(a); // Stores the string to be tokenized
        tokenizer = new StringTokenizer(tempStore, delims); // Tokenization of String with given delims
        tempName = tokenizer.nextToken(); // Gets the school name in the list of tokens to be added to the School object
        tempAddress = tokenizer.nextToken(); // Gets the Address to be added to the School
        tempCompLevel = tokenizer.nextToken(); // Gets the competitive level to be added to the school
        tempDistance = Float.parseFloat(tokenizer.nextToken()); // Gets the distance to the host.
        tempLatitude = BigDecimal.valueOf(Double.parseDouble(tokenizer.nextToken())); //gets latitude of school
        tempLongitude = BigDecimal.valueOf(Double.parseDouble(tokenizer.nextToken())); // gets longitude of the school

        if (tempName.equals(tempHost)) {
          this.isHost = true;
        }

        schools.add(new School(tempName, tempAddress, isHost, tempCompLevel, tempDistance, tempLatitude, tempLongitude));
      }
      this.regionals.get(i).setParticipants(schools);
      this.regionals.get(i).setHost(tempHost);
    }
  }

  private void populateSemiState() {
    this.semistates = new ArrayList<SemiState>(); // initializes ArrayList of SemiState meets

    for (int i = 0; i < this.semistateNum; i++) {
      this.semistates.add(new SemiState(i + 1)); // Initializes a new Regional

      this.tempAverageDistance = database.loadSemiStateAverage(i + 1); // Adds average distance to sectional
      semistates.get(i).setAverageDistance(tempAverageDistance);

      this.tempMaxDistance = database.loadSemiStateMax(i + 1); // Adds farthest distance to sectional
      semistates.get(i).setFarthestDistance(tempMaxDistance);
      
      this.tempMeet = database.loadSemiStateData(i + 1); // retrieves the SemiState Schools and stores them temporarily
      this.schools = new ArrayList<School>(); // temporary ArrayList of schools to assign to each SemiState
      this.tempHost = this.database.loadSemiStateHosts(i + 1);

      for (int a = 0; a < tempMeet.size(); a++) {
        this.isHost = false;
        tempStore = tempMeet.get(a); // Stores the string to be tokenized
        tokenizer = new StringTokenizer(tempStore, delims); // Tokenization of String with given delims
        tempName = tokenizer.nextToken(); // Gets the school name in the list of tokens to be added to the School object
        tempAddress = tokenizer.nextToken(); // Gets the Address to be added to the School
        tempCompLevel = tokenizer.nextToken(); // Gets the competitive level to be added to the school
        tempDistance = Float.parseFloat(tokenizer.nextToken()); // Gets the distance to the host.
        tempLatitude = BigDecimal.valueOf(Double.parseDouble(tokenizer.nextToken())); //gets latitude of school
        tempLongitude = BigDecimal.valueOf(Double.parseDouble(tokenizer.nextToken())); // gets longitude of the school

        if (tempName.equals(tempHost)) {
          this.isHost = true;
        }

        schools.add(new School(tempName, tempAddress, isHost, tempCompLevel, tempDistance, tempLatitude, tempLongitude));
      }
      this.semistates.get(i).setParticipants(schools);
      this.semistates.get(i).setHost(tempHost);
    }
  }

  private void populateStates() {
    this.states = new ArrayList<State>(); // initializes ArrayList of SemiState meets

    for (int i = 0; i < this.stateNum; i++) {
      this.states.add(new State(i + 1)); // Initializes a new SemiState

      this.tempAverageDistance = 0;// Adds average distance to sectional
      states.get(i).setAverageDistance(tempAverageDistance);

      this.tempMaxDistance = 0; // Adds farthest distance to sectional
      states.get(i).setFarthestDistance(tempMaxDistance);

      this.tempMeet = database.loadStateData(i + 1); // retrieves the SemiState Schools and stores them temporarily
      this.schools = new ArrayList<School>(); // temporary ArrayList of schools to assign to each SemiState

      for (int a = 0; a < tempMeet.size(); a++) {
        this.isHost = false;
        tempStore = tempMeet.get(a); // Stores the string to be tokenized
        tokenizer = new StringTokenizer(tempStore, delims); // Tokenization of String with given delims
        tempName = tokenizer.nextToken(); // Gets the school name in the list of tokens to be added to the School object
        tempAddress = tokenizer.nextToken(); // Gets the Address to be added to the School
        tempCompLevel = tokenizer.nextToken(); // Gets the competitive level to be added to the school
        tempLatitude = BigDecimal.valueOf(Double.parseDouble(tokenizer.nextToken())); //gets latitude of school
        tempLongitude = BigDecimal.valueOf(Double.parseDouble(tokenizer.nextToken())); // gets longitude of the school

        schools.add(new School(tempName, tempAddress, isHost, tempCompLevel, 0, tempLatitude, tempLongitude));

      }
      this.states.get(i).setParticipants(schools);
    }
  }

  public void updateExport() {
    this.exm = new ExportMeets(this);
  }
  
  public ArrayList<Sectional> getSectionals() {
    return sectionals;
  }
  
  public Sectional getSectionals(int position) {
    return sectionals.get(position);
  }
  
  public ArrayList<Regional> getRegionals() {
    return regionals;
  }
  
  public Regional getRegionals(int position) {
    return regionals.get(position);
  }

  public ArrayList<SemiState> getSemistates() {
    return semistates;
  }
  
  public SemiState getSemistates(int position) {
    return semistates.get(position);
  }

  public ArrayList<State> getStates() {
    return states;
  }


  public State getStates(int position) {
    return states.get(position);
  }

  public int getSectionalNum() {
    return sectionalNum;
  }

  public void setSectionalNum(int sectionalNum) {
    this.sectionalNum = sectionalNum;
  }

  public int getRegionalNum() {
    return regionalNum;
  }

  public void setRegionalNum(int regionalNum) {
    this.regionalNum = regionalNum;
  }

  public int getSemistateNum() {
    return semistateNum;
  }

  public void setSemistateNum(int semistateNum) {
    this.semistateNum = semistateNum;
  }

  public int getStateNum() {
    return stateNum;
  }

  public void setStateNum(int stateNum) {
    this.stateNum = stateNum;
  }

  public int getCompLevelOptions() {
    return compLevelOptions;
  }

  public String sectionalHosts() {
    String list = "";
    
    for(int i = 0; i < sectionals.size(); i++) {
      list += sectionals.get(i).getHost() + ";";
    }
    
    return list;
  }
  
  public String regionalHosts() {
    String list = "";
    
    for(int i = 0; i < regionals.size(); i++) {
      list += regionals.get(i).getHost() + ";";
    }
    
    return list;
  }
  
  public String semistateHosts() {
    String list = "";
    
    for(int i = 0; i < semistates.size(); i++) {
      list += semistates.get(i).getHost() + ";";
    }
    
    return list;
  }

  public ExportMeets getExm() {
    return exm;
  }

 public void importMeets() {
   this.sectionals.clear();
   this.regionals.clear();
   this.semistates.clear();
   this.states.clear();
   
   
 }
  
}
