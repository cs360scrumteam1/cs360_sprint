package modelTest;

import static org.junit.jupiter.api.Assertions.*;
import java.math.BigDecimal;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import model.meets.School;

class SchoolTest {

  private String name; // String Name of the School
  private String address; // String address of the school
  private boolean host; // Host school = true, Not Host = False.
  private String compLevel; // Level of competition
  private float distanceToHost;
  private BigDecimal latitude;
  private BigDecimal longitude;
  
  School sch = new School(name, address, host, compLevel, distanceToHost, latitude, latitude);
  
  @Test
  void getName_getter_test() {
    
    String expectname = null;//"Hammond";
    sch.getName();
    
    assertEquals(expectname, sch.getName());

  }
  
  @Test
  void setName_setter_test() {
    
    String expectname = "Hammond";
    sch.setName("Hammond");
    String result = sch.getName();
    assertEquals(expectname, result);
  
  }
  
  @Test
  void TestgetAddress() {
    String expectAddress = null;
  
    assertEquals(expectAddress, sch.getAddress()); 
  }
  
  @Test
  void TestsetAddress() {
    String expectAddress = "";
    sch.setAddress(expectAddress);
    String result = sch.getAddress();
    
    assertEquals(expectAddress,result);
  }
  
  @Test
  void Test_isHost_getter() {
    boolean temp = false;
    
    assertEquals(temp, sch.isHost());
    
    
  }
  
  @Test
  void TestsetHost() {
    boolean temp = false;
    sch.setHost(temp);
    boolean result = sch.isHost();
    
    assertEquals(temp,result);
    
  }
  
  @Test
  void TestgetCompLevel() {
    
    String expectCompLv = null;
    assertEquals(expectCompLv, sch.getCompLevel());
    
  }
  
  @Test
  void TestsetCompLevel() {
    
    String expectCompLv = "";
    sch.setCompLevel(expectCompLv);
    String result = sch.getCompLevel();
    
    assertEquals(expectCompLv,result);
    
  }
  
  @Test
  void getDistanceToHost() {
    float temp = 0.0f;
    assertEquals(temp, sch.getDistanceToHost());
  }
  
  @Test 
  void setDistanceToHost() {
    
    float temp = 0.4f;
    sch.setDistanceToHost(temp);
    float result = sch.getDistanceToHost();
    
    assertEquals(temp,result);
    
  }
  
  @Test
  void getLatitude() {
    
    BigDecimal temp = new BigDecimal(0.0);
    assertNotEquals(temp, sch.getLatitude());
    
  }
  
  @Test
  void setLatitude() {
    BigDecimal temp = new BigDecimal(3.0);
    sch.setLatitude(temp);
    BigDecimal result = sch.getLatitude();
    
    assertEquals(temp, result);
    
  }
  
  @Test
  void getLongitude() {
    BigDecimal temp = new BigDecimal(0.0);
    assertNotEquals(temp, sch.getLongitude());
  }
  
  @Test
  void setLongitude() {
    BigDecimal temp = new BigDecimal(3.0);
    sch.setLongitude(temp);
    BigDecimal result = sch.getLongitude();
    
    assertEquals(temp, result);
    
  }

}
