package modelTest;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import model.database.MeetData;       

public class MeetDataTest {
  
  private final String DB_NAME = "School"; // schema name
  private final String DB_USERNAME = "team9";
  private final String DB_PASSWORD = "1234567890";
  private final String DB_HOSTNAME = "sprintinstance.cvdjhxgkq530.us-east-2.rds.amazonaws.com"; // Endpoint
  private final String DB_PORT = "3306";
  private final String driverName = "com.mysql.jdbc.Driver";
  private final String jdbcUrl = "jdbc:mysql://" + DB_HOSTNAME + ":" + DB_PORT + "/" + DB_NAME + "?user="
          + DB_USERNAME + "&password=" + DB_PASSWORD;

  private Connection conn;
  private PreparedStatement preparedStatement;
  private Statement stat;
  private ResultSet rset;
  private ArrayList<String> data;
  private int num;
  private String temp;
  private float tempAverage;
  private float tempMax;
  private BigDecimal tempLatitude;
  private BigDecimal tempLongtitude;


  @Test
  void testNumSectionals() {
    
    num = 0;
    int NumSectionalExpection = 32;
    try {
        String query = "select * from repository order by sectiontype desc limit 1";

        conn = DriverManager.getConnection(jdbcUrl);
        preparedStatement = conn.prepareStatement(query);
        rset = preparedStatement.executeQuery(query);

        while (rset.next()) {
            temp = rset.getString("sectiontype");
            num = Integer.parseInt(temp);
        }  
    }
        catch (SQLException e) {
          System.out.println("query failed 1" + e.getStackTrace());
      } 
 
    assertTrue(NumSectionalExpection==num);
  }

  @Test
  void testNumRegionals() {
    num = 0;
    int NumRegionalExpection = 16;
    try {
        String query = "select * from repository order by regiontype desc limit 1";

        conn = DriverManager.getConnection(jdbcUrl);
        preparedStatement = conn.prepareStatement(query);
        rset = preparedStatement.executeQuery(query);

        while (rset.next()) {
            temp = rset.getString("regiontype");
            num = Integer.parseInt(temp);
        }  
    }
        catch (SQLException e) {
          System.out.println("query failed 1" + e.getStackTrace());
      } 
    assertTrue(NumRegionalExpection==num);
  }

  @Test
  void testNumSemiStates() {
    num = 0;
    int SemiStateNumExpectation=4;
    try {
        String query = "select * from repository order by semistatetype desc limit 1";

        conn = DriverManager.getConnection(jdbcUrl);
        preparedStatement = conn.prepareStatement(query);
        rset = preparedStatement.executeQuery(query);

        while (rset.next()) {
            temp = rset.getString("semistatetype");
            num = Integer.parseInt(temp);

        }

    } catch (SQLException e) {
        System.out.println("query failed 3" + e.getStackTrace());
    } 
    assertTrue(SemiStateNumExpectation==num);
  }

  @Test
  void testNumStates() {
    num = 0;
    int StateNumExpectation=1;
    try {
        String query = "select * from repository order by statetype desc limit 1";

        conn = DriverManager.getConnection(jdbcUrl);
        preparedStatement = conn.prepareStatement(query);
        rset = preparedStatement.executeQuery(query);

        while (rset.next()) {
            temp = rset.getString("statetype");
            num = Integer.parseInt(temp);
        }

    } catch (SQLException e) {
        System.out.println("query failed 4" + e.getStackTrace());
    } 
    assertTrue(StateNumExpectation==num);
  
  }

  @Test
  void testLoadSectionalHosts() {
    
    MeetData meetdata = new MeetData();
    for (int i=1; i<= meetdata.numSectionals(); i++) {
      int sectiontype = i;
    
      temp = "";
      String SectionHost = null;
    try {
        String query = "select schoolname from repository where sectionhost=1 and sectiontype=" + sectiontype;
        conn = DriverManager.getConnection(jdbcUrl);
        preparedStatement = conn.prepareStatement(query);
        rset = preparedStatement.executeQuery(query);

        while (rset.next()) {
            temp = (rset.getString("schoolname"));
            SectionHost = temp;
        }
    } catch (SQLException e) {
        System.out.println("query failed 5" + e.getStackTrace());
    } 
      assertNotNull(SectionHost);
    }
  }

  @Test
  void testLoadRegionalHosts() {
    
    MeetData meetdata = new MeetData();
    for (int i = 1; i <= meetdata.numRegionals(); i++) {
      int regiontype=i;
      temp = "";
      String regionHost = null;
    try {
        String query = "select schoolname from repository where regionhost=1 and regiontype=" + regiontype;
        conn = DriverManager.getConnection(jdbcUrl);
        preparedStatement = conn.prepareStatement(query);
        rset = preparedStatement.executeQuery(query);

        while (rset.next()) {
            temp = (rset.getString("schoolname"));
            regionHost = temp;
        }
    } catch (SQLException e) {
        System.out.println("query failed 6" + e.getStackTrace());
    } 
   assertNotNull(regionHost);
  }
  }

  @Test
  void testLoadSemiStateHosts() {
    
    MeetData meetdata = new MeetData();
    for (int i =1; i<=meetdata.numSemiStates();i++) {
      int semistatetype = i;
      temp = "";
      String semistateHost = null;
    
    try {
        String query = "select schoolname from repository where semistatehost=1 and semistatetype=" + semistatetype;
        conn = DriverManager.getConnection(jdbcUrl);
        preparedStatement = conn.prepareStatement(query);
        rset = preparedStatement.executeQuery(query);

        while (rset.next()) {
            temp = (rset.getString("schoolname"));
            semistateHost = temp;
        }
    } catch (SQLException e) {
        System.out.println("query failed 7" + e.getStackTrace());
    } 
    assertNotNull(semistateHost);
    }
  }

  @Test
  void testLoadSectionalData() {
    MeetData meetdata = new MeetData();
    for (int i=1; i<= meetdata.numSectionals(); i++) {
      int sectiontype = i;
      temp = "";
      boolean CheckSectionData = false;
      data = new ArrayList<String>();
      try {

          String query = "SELECT schoolname, address, competitionlevel, sectiondistance, Latitude, Longtitude from repository where sectiontype ="
                  + sectiontype;

          conn = DriverManager.getConnection(jdbcUrl);
          preparedStatement = conn.prepareStatement(query);
          rset = preparedStatement.executeQuery(query);

          while (rset.next()) {

              temp = rset.getString("schoolname") + ";" + rset.getObject("address") + ";"
                  + rset.getString("competitionlevel")+";"+rset.getString("sectiondistance") + ";"
                  + rset.getBigDecimal("Latitude") + ";" +rset.getBigDecimal("Longtitude") + ";" ;
              
              CheckSectionData = data.add(temp);

          }
      }
      catch (SQLException e) {
        System.out.println("query failed 8" + e.getStackTrace());
    }
      assertTrue(CheckSectionData);
    }
  }
 

  @Test
  void testLoadRegionalData() {
    MeetData meetdata = new MeetData();
    for (int i=1; i<= meetdata.numRegionals(); i++) {
      int regiontype = i;
      temp = "";
      boolean CheckRegionData = false;
      data = new ArrayList<String>();
    try {

        String query = "SELECT schoolname, address, competitionlevel, regiondistance, Latitude, Longtitude from repository where regiontype ="
                + regiontype;

        conn = DriverManager.getConnection(jdbcUrl);
        stat = conn.createStatement();
        rset = stat.executeQuery(query);

        while (rset.next()) {


            temp = rset.getString("schoolname") + ";" + rset.getObject("address") + ";"
                + rset.getString("competitionlevel")+";"+rset.getString("regiondistance") + ";"
                + rset.getBigDecimal("Latitude") + ";" +rset.getBigDecimal("Longtitude") + ";" ;


            CheckRegionData=data.add(temp);

        }
    }

    catch (SQLException e) {
        System.out.println("query failed 9" + e.getStackTrace());
    } 
    assertTrue(CheckRegionData);
    }
  }

  @Test
  void testLoadSemiStateData() {
    MeetData meetdata = new MeetData();
    for (int i=1; i<= meetdata.numSemiStates(); i++) {
      int semistatetype = i;
      temp = "";
      boolean CheckSemiStateData = false;
      data = new ArrayList<String>();
    try {

        String query = "SELECT schoolname, address, competitionlevel, semistatedistance, Latitude, Longtitude from repository where semistatetype ="
                + semistatetype;

        conn = DriverManager.getConnection(jdbcUrl);
        stat = conn.createStatement();
        rset = stat.executeQuery(query);

        while (rset.next()) {


            temp = rset.getString("schoolname") + ";" + rset.getObject("address") + ";"
            + rset.getString("competitionlevel")+";"+rset.getString("semistatedistance") + ";"
            + rset.getBigDecimal("Latitude") + ";" +rset.getBigDecimal("Longtitude") + ";" ;

            CheckSemiStateData=data.add(temp);
        }
    }

    catch (SQLException e) {
        System.out.println("query failed 10" + e.getStackTrace());
    } 
    assertTrue(CheckSemiStateData);
}   
  }

  @Test
  void testLoadStateData() {
    MeetData meetdata = new MeetData();
    for (int i=1; i<= meetdata.numStates(); i++) {
      int statetype = i;
    temp = "";
    boolean CheckStateData = false;
    data = new ArrayList<String>();
    try {
        String query = "SELECT schoolname, address, competitionlevel, Latitude, Longtitude From repository where statetype =" + statetype;

        conn = DriverManager.getConnection(jdbcUrl);
        stat = conn.createStatement();
        rset = stat.executeQuery(query);

        while (rset.next()) {

            temp = rset.getString("schoolname") + ";" + rset.getObject("address") + ";"
                    + rset.getString("competitionlevel") + ";"
                    + rset.getBigDecimal("Latitude") + ";" +rset.getBigDecimal("Longtitude") + ";" ;
            CheckStateData = data.add(temp);
        }
    }

    catch (SQLException e) {
        System.out.println("query failed 11" + e.getStackTrace());
    } 
    assertTrue(CheckStateData);
    }
  }

  @Test
  void testLoadSectionalAverage() {
    fail("Not yet implemented");
  }

  @Test
  void testLoadRegionalAverage() {
    fail("Not yet implemented");
  }

  @Test
  void testLoadSemiStateAverage() {
    fail("Not yet implemented");
  }

  @Test
  void testLoadSectionalMax() {
    fail("Not yet implemented");
  }

  @Test
  void testLoadRegionalMax() {
    fail("Not yet implemented");
  }

  @Test
  void testLoadSemiStateMax() {
    fail("Not yet implemented");
  }

  @Test
  void testLoadLatitude() {
    MeetData meetdata = new MeetData();
    for (int i=1; i<= meetdata.numSectionals(); i++) {
      int sectiontype = i;
      float CheckLatitude = 0;
    tempLatitude = new BigDecimal(0.1);
    try {
        String query = "SELECT Latitude From repository where sectiontype = " + sectiontype;

        conn = DriverManager.getConnection(jdbcUrl);
        stat = conn.createStatement();
        rset = stat.executeQuery(query);

        while (rset.next()) {
          tempAverage = Float.parseFloat(rset.getString("Latitude"));
          
        }
    }
    catch (SQLException e) {
        System.out.println("query failed 19" + e.getStackTrace());
    } 
    assertTrue(tempAverage!=CheckLatitude);
    }
  }

  @Test
  void testLoadLongitude() {
    
    MeetData meetdata = new MeetData();
    for (int i=1; i<= meetdata.numSectionals(); i++) {
      int sectiontype = i;
      float CheckLongitude = 0;
    tempLongtitude = new BigDecimal(0.1);
    try {
        String query = "SELECT Longtitude From repository where sectiontype" + sectiontype;

        conn = DriverManager.getConnection(jdbcUrl);
        stat = conn.createStatement();
        rset = stat.executeQuery(query);

        while (rset.next()) {
          tempAverage = Float.parseFloat(rset.getString("Longtitude"));
         
        }
    }

    catch (SQLException e) {
        System.out.println("query failed 20" + e.getStackTrace());
    } 
    assertTrue(tempAverage!=CheckLongitude);
      }
 }
  
}

