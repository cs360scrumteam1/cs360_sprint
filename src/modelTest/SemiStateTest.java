package modelTest;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import model.database.MeetData;
import model.meets.Regional;
import model.meets.SemiState;

class SemiStateTest {

  @Test
  void SemiState_setter_test() {
    MeetData meetdata = new MeetData();
    
    for (int id=0; id<= meetdata.numSemiStates(); id++ ) {
      SemiState ss = new SemiState(id);
      ss.setId(id);
      
      assertEquals(ss.getId(), id);
    }
    
  }
 }

