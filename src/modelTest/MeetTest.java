package modelTest;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import model.meets.Meet;
import model.meets.School;

class MeetTest {
  
  private String title;
  private int id;
  private float averageDistance;
  private float farthestDistance;
  private ArrayList<School> participants;
  private String host;
  
  Meet m = new Meet();

  @Test
  void testgetTitle() {
    String temp = null;
    assertNotEquals(temp, m.getTitle());
   
  }
  
  @Test
  void testsetTitle() {
    String temp = "";
    m.setTitle(temp);
    String tempTitle = m.getTitle();
    assertEquals(temp, tempTitle);
    
  }
  
  @Test
  void testgetId() { 
    int temp = 0;
    assertEquals(temp, m.getId());
    
  }
  
  @Test
  void testsetId() {
    int temp = 0;
    m.setId(temp);
    int result = m.getId();
    assertEquals(temp, result);
    
  }
  
  @Test
  void testgetParticipants() {
    ArrayList<School> actual = participants;
    actual = null;
    int position = 2;
    ArrayList<School> expected = m.getParticipants();
    
    Assert.assertEquals(expected, actual);
    
    
  }
  
  @Test
  void testsetParticipants() {
   
    ArrayList<School> temp = participants;
    temp = null;
    ArrayList<School> result = m.getParticipants();
    
    Assert.assertEquals(result, temp);
    
    
  }
  
  @Test
  void testgetAverageDistance() {
    
    float actual = 0.0f;
    assertEquals(actual, m.getAverageDistance());
    
  }
  
  @Test
  void testsetAverageDistance() {
    
    float temp = 0.4f;
    m.setAverageDistance(temp);
    float result = m.getAverageDistance();
    
    assertEquals(temp, result);
    
  }
  
  @Test
  void testgetFarthestDistance() {
    
    float actual = 0.0f;
    assertEquals(actual, m.getFarthestDistance());
    
  }
  
  @Test
  void testsetFarthestDistance() {
    
    float temp = 0.4f;
    m.setFarthestDistance(temp);
    float result = m.getFarthestDistance();
    
    assertEquals(temp, result); 
    
  }
  
  @Test
  void testtoString() {
    
    String temp = "";
    assertEquals(temp, m.toString());
    
  }
  
  @Test
  void testgetHost() {
    
    String temp = "";
    assertNotEquals(temp, m.getHost());
  }
  
  @Test
  void testsetHost() {
    
    String temp = "";
    m.setHost(temp);
    String result = m.getHost();
    assertEquals(temp, result);
    
    
  }
}