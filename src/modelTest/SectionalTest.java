package modelTest;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import model.database.MeetData;
import model.meets.Regional;
import model.meets.Sectional;

class SectionalTest {

  @Test
  void Sectional_setter_test() {
  MeetData meetdata = new MeetData();
    
    for (int id=0; id<= meetdata.numSectionals(); id++ ) {
      Sectional sl = new Sectional(id);
      sl.setId(id);
      
      assertEquals(sl.getId(), id);
    }
    
  }

}
