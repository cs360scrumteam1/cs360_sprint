package modelTest;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import model.database.MeetData;
import model.meets.Regional;

class RegionalTest {

  @Test
  void Regional_setter_test() {
    
    MeetData meetdata = new MeetData();
    
    for (int id=0; id<= meetdata.numRegionals(); id++ ) {
      Regional r = new Regional(id);
      r.setId(id);
      
      assertEquals(r.getId(), id);
    }
    
  }

}
