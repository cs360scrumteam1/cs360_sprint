package modelTest;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import model.database.MeetData;
import model.meets.Regional;
import model.meets.State;

class StateTest {

  @Test
  void State_setter_test() {
    MeetData meetdata = new MeetData();
    
    for (int id=0; id<= meetdata.numStates(); id++ ) {
      State s = new State(id);
      s.setId(id);
      
      assertEquals(s.getId(), id);
    }
    
  }
  }


