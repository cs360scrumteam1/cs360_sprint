package modelTest;

//import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;


import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.hamcrest.CoreMatchers.is;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import org.junit.Assert;
//import org.junit.Test;
import model.database.MeetData;
import model.database.PopulateMeets;
import model.meets.Meet;
import model.meets.Regional;
import model.meets.School;
import model.meets.Sectional;
import model.meets.SemiState;
import model.meets.State;

public class PopulateMeetsTest {
  
  private MeetData database;
  private ArrayList<School> schools;
  private ArrayList<Sectional> sectionals;
  private ArrayList<Regional> regionals;
  private ArrayList<SemiState> semistates;
  private ArrayList<State> states;
  private ArrayList<String> tempMeet;
  private int sectionalNum;
  private int regionalNum;
  private int semistateNum;
  private int stateNum;
  private BigDecimal tempLatitude;
  private BigDecimal tempLongitude;
  private final int compLevelOptions = 3;
  private String tempHost;
  private String tempName;
  private String  tempAddress;
  private String tempCompLevel; 
  private String tempStore;
  private float tempDistance;
  private float tempAverageDistance;
  private float tempMaxDistance;
  private boolean isHost;
  private StringTokenizer tokenizer;
  private final String delims = ";";
  
  PopulateMeets p = new PopulateMeets();

  @Test
  public void testGetSectionals() {
   
   ArrayList<Sectional> actual = sectionals;
   actual = null;
   ArrayList<Sectional> expected = p.getSectionals(); //sectional 
    
   Assert.assertNotEquals(expected, actual);

  }

  @Test
  public void testGetSectionalsInt() {
    ArrayList<Sectional> actual = sectionals;
    actual = null;
    int position = 2; 
    Sectional expected = p.getSectionals(position);
    
    Assert.assertNotEquals(expected, actual);
  }

  @Test
  public void testGetRegionals() {
    ArrayList<Regional> actual = regionals;
    actual = null;
    ArrayList<Regional> expected = p.getRegionals(); 
     
    Assert.assertNotEquals(expected, actual);
  }

  @Test
  public void testGetRegionalsInt() {
    ArrayList<Regional> actual = regionals;
    actual = null;
    int position = 2; 
    Regional expected = p.getRegionals(position);
    
    Assert.assertNotEquals(expected, actual);
  }

  @Test
  public void testGetSemistates() {
    ArrayList<SemiState> actual = semistates;
    actual = null;
    ArrayList<SemiState> expected = p.getSemistates(); 
     
    Assert.assertNotEquals(expected, actual);
  }
 

  @Test
  public void testGetSemistatesInt() {
    ArrayList<SemiState> actual = semistates;
    actual = null;
    int position = 2; 
    SemiState expected = p.getSemistates(position);
    
    Assert.assertNotEquals(expected, actual);
  }

  @Test
  public void testGetStates() {
    ArrayList<State> actual = states;
    actual = null;
    ArrayList<State> expected = p.getStates();
     
    Assert.assertNotEquals(expected, actual);
  }

  @Test
  public void testGetStatesInt() {
    ArrayList<State> actual = states;
    actual = null;
    int position = 0; 
    State expected = p.getStates(position);
    
    Assert.assertNotEquals(expected, actual);
  }

  @Test
  public void testGetSectionalNum() {
    
    int num = 32; 
    p.getSectionalNum();
    
    assertEquals(num, p.getSectionalNum());
  }

  @Test
  public void testSetSectionalNum() {
    
   int num = 20;
   p.setSectionalNum(num);
   int expected = p.getSectionalNum();
   
   Assert.assertEquals(num, expected);
      
  }

  @Test
  public void testGetRegionalNum() {
    int num = 16;
    p.getRegionalNum();
    
    assertEquals(num, p.getRegionalNum());
  }

  @Test
  public void testSetRegionalNum() {
    int num = 10;
    p.setRegionalNum(num);
    int expected = p.getRegionalNum();
    
    Assert.assertEquals(num, expected);
  }

  @Test
  public void testGetSemistateNum() {
    int num = 4;
    p.getSemistateNum();
    
    assertEquals(num, p.getSemistateNum());
  }

  @Test
  public void testSetSemistateNum() {
    int num = 3;
    p.setSemistateNum(num);
    int expected = p.getSemistateNum();
    
    Assert.assertEquals(num, expected);
  }

  @Test
  public void testGetStateNum() {
    int num = 1;
    p.getStateNum();
    
    assertEquals(num, p.getStateNum());
  }

  @Test
  public void testSetStateNum() {
    int num = 1;
    p.setStateNum(num);
    int expected =p.getStateNum();
    
    Assert.assertEquals(num, expected);
  }

  @Test
  public void testGetCompLevelOptions() {
    int num = 3;
    p.getCompLevelOptions();
    
    assertEquals(num, p.getCompLevelOptions());
  }

  @Test
  public void testSectionalHosts() {
    String temp = null;
    p.sectionalHosts();
    
    assertNotEquals(temp, p.sectionalHosts());
    
  }

  @Test
  public void testRegionalHosts() {
    String temp = null;
    p.regionalHosts();
    
    assertNotEquals(temp, p.regionalHosts());
  }

  @Test
  public void testSemistateHosts() {
    String temp = null;
    p.semistateHosts();
    
    assertNotEquals(temp, p.semistateHosts());
  }

}
