package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import view.MainView;
import view.tablecomponents.Table;

public class Listener {
  private MainView mainView;
  private Object actSource;
  private ActionListener actionL;
  private ArrayList<Table> tempTableList;
  private Table tempTable;


  public Listener(MainView main) {
    this.mainView = main;
    this.actionL = new Actionlistener(); // initializes an ActionListener

    // Adds the hostpanel objects to the action listener
    mainView.getHp().getHst().getBtnSwapSelected().addActionListener(actionL);
    mainView.getHp().getHst().getMeetNumbersCb().addActionListener(actionL);
    mainView.getHp().getHst().getMeetTypeCb().addActionListener(actionL);
    mainView.getHp().getHlt().getBtnAddChost().addActionListener(actionL);
    mainView.getHp().getHlt().getBtnApHost().addActionListener(actionL);
    mainView.getHp().getHlt().getBtnRchost().addActionListener(actionL);
    mainView.getHp().getHlt().getBtnRpHost().addActionListener(actionL);

    // Adds menu items to the action listener
    mainView.getHostMenuItem().addActionListener(actionL);
    mainView.getMapMenuItem().addActionListener(actionL);
    mainView.getTableMenuItem().addActionListener(actionL);
    mainView.getSaveMenuItem().addActionListener(actionL);
    mainView.getLoadMenuItem().addActionListener(actionL);

    // Adds the view buttons from the Frame class to the action listener
    mainView.getTabs()[0].addActionListener(actionL);
    mainView.getTabs()[1].addActionListener(actionL);
    mainView.getTabs()[2].addActionListener(actionL);

    // Adds the buttons from the TableComponents Package to the action listener
    tempTableList = mainView.getTableView().getSectionals();
    for (int i = 0; i < tempTableList.size(); i++) {
      tempTable = tempTableList.get(i);
      tempTable.getTableOptionsView().getBtnAddSchool().addActionListener(actionL);
      tempTable.getTableOptionsView().getBtnEditSchoolInfo().addActionListener(actionL);
      tempTable.getTableOptionsView().getBtnSaveEditedInformation().addActionListener(actionL);
    }
    tempTableList = mainView.getTableView().getRegionals();
    for (int i = 0; i < tempTableList.size(); i++) {
      tempTable = tempTableList.get(i);
      tempTable.getTableOptionsView().getBtnAddSchool().addActionListener(actionL);
      tempTable.getTableOptionsView().getBtnEditSchoolInfo().addActionListener(actionL);
      tempTable.getTableOptionsView().getBtnSaveEditedInformation().addActionListener(actionL);
    }
    tempTableList = mainView.getTableView().getSemiStates();
    for (int i = 0; i < tempTableList.size(); i++) {
      tempTable = tempTableList.get(i);
      tempTable.getTableOptionsView().getBtnAddSchool().addActionListener(actionL);
      tempTable.getTableOptionsView().getBtnEditSchoolInfo().addActionListener(actionL);
      tempTable.getTableOptionsView().getBtnSaveEditedInformation().addActionListener(actionL);
    }
    tempTableList = mainView.getTableView().getStates();
    for (int i = 0; i < tempTableList.size(); i++) {
      tempTable = tempTableList.get(i);
      tempTable.getTableOptionsView().getBtnAddSchool().addActionListener(actionL);
      tempTable.getTableOptionsView().getBtnEditSchoolInfo().addActionListener(actionL);
      tempTable.getTableOptionsView().getBtnSaveEditedInformation().addActionListener(actionL);
    }

    // Add the JComboBoxes from Map options and other components from the class to the action
    // listener
    mainView.getMapOptView().getCbSelectionType().addActionListener(actionL);
    mainView.getMapOptView().getCbViewingParty().addActionListener(actionL);
  }

  class Actionlistener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      actSource = e.getSource();

      if (actSource == mainView.getTabs()[0] || actSource == mainView.getTableMenuItem()) {
        mainView.buildTableView();
      } else if (actSource == mainView.getTabs()[1] || actSource == mainView.getMapMenuItem()) {
        mainView.buildMapView();
      } else if (actSource == mainView.getTabs()[2] || actSource == mainView.getHostMenuItem()) {
        mainView.buildHostPanelView();
      } else if (actSource == tempTable.getTableOptionsView().getBtnAddSchool()) {
        System.out.println("Add Schools Button is Operational");
      } else if (actSource == tempTable.getTableOptionsView().getBtnEditSchoolInfo()) {
        System.out.println("Edit Schools Button is Operational");
      } else if (actSource == tempTable.getTableOptionsView().getBtnSaveEditedInformation()) {
        System.out.println("Save Information Button is Operational");
      } else if (actSource == mainView.getMapOptView().getCbSelectionType()) {
        mainView.getMapOptView().changeViewingParty();
      } else if (actSource == mainView.getMapOptView().getCbViewingParty()) {
        mainView.getMapOptView().updateMap(mainView.getMap());
      } else if (actSource == mainView.getSaveMenuItem()) {
        mainView.getPop().updateExport();
        mainView.getPop().getExm().export();
      } else if (actSource == mainView.getLoadMenuItem()) {
        System.out.println("Has not been implemented");
      } else if (actSource == mainView.getHp().getHst().getBtnSwapSelected()) {
        try {
          String oldHost =
              (String) mainView.getHp().getHst().getCurrentHostList().getSelectedValue();
          String newHost =
              (String) mainView.getHp().getHst().getSelectedMeetList().getSelectedValue();
          String meetTitle =
              (String) mainView.getHp().getHst().getMeetNumbersCb().getSelectedItem().toString();
          mainView.popUpdateHosts(oldHost, newHost, meetTitle);
          mainView.getHp().getHst().swapHost();
        } catch (Exception nulPointerExceptiion) {
          JOptionPane.showMessageDialog(null,
              "You must select a Meet and school to be swapped with the current host!",
              "Invalid Operation", JOptionPane.WARNING_MESSAGE);
          return;
        }
      } else if (actSource == mainView.getHp().getHst().getMeetTypeCb()) {
        mainView.getHp().getHst().changeViewingParty();
      } else if (actSource == mainView.getHp().getHst().getMeetNumbersCb()) {
        mainView.getHp().getHst().updateLists();
      } else if (actSource == mainView.getHp().getHlt().getBtnAddChost()) {
        mainView.getHp().getHlt().addChost();
      } else if (actSource == mainView.getHp().getHlt().getBtnApHost()) {
        System.out.println("useless");
      } else if (actSource == mainView.getHp().getHlt().getBtnRchost()) {
        mainView.getHp().getHlt().removeChost();
      } else if (actSource == mainView.getHp().getHlt().getBtnRpHost()) {
        System.out.println("useless");
      }

    }

  }
}
