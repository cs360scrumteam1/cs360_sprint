package view.hostcomponents;

import java.awt.Font;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import model.database.PopulateMeets;
import model.meets.School;

public class HostListTab extends JPanel {
  private PopulateMeets pop;
  private JTable currentTbl;
  private JTable potentialTbl;
  private GridBagLayout gridBagLayout;
  private JButton btnAddChost;
  private GridBagConstraints gbcA;
  private JButton btnRchost;
  private GridBagConstraints gbcB;
  private JButton btnRpHost;
  private GridBagConstraints gbcC;
  private JButton btnApHost;
  private GridBagConstraints gbcD;
  private JScrollPane currentSp;
  private GridBagConstraints gbcE;
  private JScrollPane potentialSp;
  private GridBagConstraints gbcF;
  private int size;
  private final int columns = 2;
  private StringTokenizer tokenizer;
  private final String delims = ";";
  private final String[] ccolNames = {"Current Hosts", "Meet Assignment"};
  private final String[] pcolNames = {"Potential Hosts", "Meet Assignment"};
  public String[][] infoA;
  public String[][] infoB;
  private int count;
  
  public HostListTab(PopulateMeets popTemp) {
    this.pop = popTemp;
    gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[]{45, 91, 195, 186, 45, 45, 132, 140, 159, 45, 0};
    gridBagLayout.rowHeights = new int[]{0, 0, 0, 0};
    gridBagLayout.columnWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);
    
    btnAddChost = new JButton("Add Current Host");
    btnAddChost.setFont(new Font("Times New Roman", Font.PLAIN, 20));
    gbcA = new GridBagConstraints();
    gbcA.anchor = GridBagConstraints.NORTH;
    gbcA.insets = new Insets(0, 0, 5, 5);
    gbcA.gridx = 1;
    gbcA.gridy = 1;
    add(btnAddChost, gbcA);
    
    btnRchost = new JButton("Remove Current Host");
    btnRchost.setFont(new Font("Times New Roman", Font.PLAIN, 20));
    gbcB = new GridBagConstraints();
    gbcB.gridwidth = 2;
    gbcB.insets = new Insets(0, 0, 5, 5);
    gbcB.gridx = 2;
    gbcB.gridy = 1;
    add(btnRchost, gbcB);
    
    btnRpHost = new JButton("Remove Potential Host");
    btnRpHost.setFont(new Font("Times New Roman", Font.PLAIN, 20));
    gbcC = new GridBagConstraints();
    gbcC.insets = new Insets(0, 0, 5, 5);
    gbcC.gridx = 6;
    gbcC.gridy = 1;
    add(btnRpHost, gbcC);
    
    btnApHost = new JButton("Add Potential Host");
    btnApHost.setFont(new Font("Times New Roman", Font.PLAIN, 20));
    gbcD = new GridBagConstraints();
    gbcD.gridwidth = 2;
    gbcD.insets = new Insets(0, 0, 5, 5);
    gbcD.gridx = 7;
    gbcD.gridy = 1;
    add(btnApHost, gbcD);
    
    count = 0;
    
    currentSp = new JScrollPane();
    gbcE = new GridBagConstraints();
    gbcE.gridwidth = 3;
    gbcE.insets = new Insets(0, 0, 0, 5);
    gbcE.fill = GridBagConstraints.BOTH;
    gbcE.gridx = 1;
    gbcE.gridy = 2;
    add(currentSp, gbcE);
    
    initCurrentTable();
    currentSp.setViewportView(currentTbl);
    
    potentialSp = new JScrollPane();
    gbcF = new GridBagConstraints();
    gbcF.insets = new Insets(0, 0, 0, 5);
    gbcF.gridwidth = 3;
    gbcF.fill = GridBagConstraints.BOTH;
    gbcF.gridx = 6;
    gbcF.gridy = 2;
    add(potentialSp, gbcF);
    
    initPotentialTable();
    potentialSp.setViewportView(potentialTbl);
    this.btnApHost.setVisible(false);
    this.btnRpHost.setVisible(false);
  }
  
  public void initCurrentTable() {
    String temp = pop.sectionalHosts() + pop.regionalHosts() + pop.semistateHosts();
    tokenizer = new StringTokenizer(temp, delims);
    size = tokenizer.countTokens()/2;
    infoA = new String[size*2][columns];

    for (int j = 0; j < size; j++) {
      infoA[j][0] = tokenizer.nextToken();
      infoA[j][1] = tokenizer.nextToken();
      count++;
    }
    currentTbl = new JTable(infoA, ccolNames) {
      @Override
      public boolean isCellEditable(int row, int column) {
        return false;
      }
    };
    currentTbl.setFont(new Font("TimesRoman", Font.PLAIN, 18));
    for (int k = 0; k < columns; k++) {
      currentTbl.getColumnModel().getColumn(k).setPreferredWidth(400);
    }
    currentTbl.setRowHeight(30);
    currentTbl.getTableHeader().setFont(new Font("TimesRoman", Font.PLAIN, 18));
  }
  
  public void initPotentialTable(){
    ArrayList<School> tempSchools;
    tempSchools = pop.getStates().get(0).getParticipants();
    size = tempSchools.size();
    infoB = new String[size][columns];

    for (int j = 0; j < size; j++) {
      infoB[j][0] = tempSchools.get(j).getName();
      infoB[j][1] = "Unavailable at this time!";
      
    }
    potentialTbl = new JTable(infoB, pcolNames) {
      @Override
      public boolean isCellEditable(int row, int column) {
        return false;
      }
    };
    potentialTbl.setFont(new Font("TimesRoman", Font.PLAIN, 18));
    for (int k = 0; k < columns; k++) {
      potentialTbl.getColumnModel().getColumn(k).setPreferredWidth(400);
    }
    potentialTbl.setRowHeight(30);
    currentTbl.getTableHeader().setFont(new Font("TimesRoman", Font.PLAIN, 18));
  }
  
  public void addChost() {
    int tempRow = potentialTbl.getSelectedRow();
    String tempName;
    String meetEnroll = "Available to Host";
    if(tempRow < 0) {
      JOptionPane.showMessageDialog(null,
          "Please select a potential host school from the table!",
          "Invalid Operation Error",
          JOptionPane.WARNING_MESSAGE);
      return;
    }
    count++;
    tempName = infoB[tempRow][0];
    infoA[count][0] = tempName;
    infoA[count][1] = meetEnroll;
    infoB[tempRow][0] = null;
    infoB[tempRow][1] = null;
    
    currentTbl = new JTable(infoA, ccolNames) {
      @Override
      public boolean isCellEditable(int row, int column) {
        return false;
      }
    };
    currentTbl.setFont(new Font("TimesRoman", Font.PLAIN, 18));
    for (int k = 0; k < columns; k++) {
      currentTbl.getColumnModel().getColumn(k).setPreferredWidth(400);
    }
    currentTbl.setRowHeight(30);
    currentTbl.getTableHeader().setFont(new Font("TimesRoman", Font.PLAIN, 18));
    
    potentialTbl = new JTable(infoB, pcolNames) {
      @Override
      public boolean isCellEditable(int row, int column) {
        return false;
      }
    };
    potentialTbl.setFont(new Font("TimesRoman", Font.PLAIN, 18));
    for (int k = 0; k < columns; k++) {
      potentialTbl.getColumnModel().getColumn(k).setPreferredWidth(400);
    }
    potentialTbl.setRowHeight(30);
    currentTbl.getTableHeader().setFont(new Font("TimesRoman", Font.PLAIN, 18));
    currentTbl.validate();
    potentialTbl.validate();
    potentialSp.setViewportView(potentialTbl);
    currentSp.setViewportView(currentTbl);
    this.currentSp.validate();
    this.potentialSp.validate();
    validate();
  }
  
  public void removeChost() {
    int tempRow = currentTbl.getSelectedRow();
    String tempName;
    String meetEnroll;
    if(tempRow < 0) {
      JOptionPane.showMessageDialog(null,
          "Please select a potential host school from the table!",
          "Invalid Operation Error",
          JOptionPane.WARNING_MESSAGE);
      return;
    }
    count++;
    tempName = infoA[tempRow][0];
    meetEnroll = infoA[tempRow][1];
    infoB[count][0] = tempName;
    infoB[count][1] = meetEnroll;
    infoA[tempRow][0] = null;
    infoA[tempRow][1] = null;
    
    currentTbl = new JTable(infoA, ccolNames) {
      @Override
      public boolean isCellEditable(int row, int column) {
        return false;
      }
    };
    currentTbl.setFont(new Font("TimesRoman", Font.PLAIN, 18));
    for (int k = 0; k < columns; k++) {
      currentTbl.getColumnModel().getColumn(k).setPreferredWidth(400);
    }
    currentTbl.setRowHeight(30);
    currentTbl.getTableHeader().setFont(new Font("TimesRoman", Font.PLAIN, 18));
    
    potentialTbl = new JTable(infoB, pcolNames) {
      @Override
      public boolean isCellEditable(int row, int column) {
        return false;
      }
    };
    potentialTbl.setFont(new Font("TimesRoman", Font.PLAIN, 18));
    for (int k = 0; k < columns; k++) {
      potentialTbl.getColumnModel().getColumn(k).setPreferredWidth(400);
    }
    potentialTbl.setRowHeight(30);
    currentTbl.getTableHeader().setFont(new Font("TimesRoman", Font.PLAIN, 18));
    currentTbl.validate();
    potentialTbl.validate();
    potentialSp.setViewportView(potentialTbl);
    currentSp.setViewportView(currentTbl);
    this.currentSp.validate();
    this.potentialSp.validate();
    validate();
  }
  
  public void updatePop(PopulateMeets pop) {
    this.pop = pop;
    initCurrentTable();
    initPotentialTable();
    validate();
  }

  public JButton getBtnAddChost() {
    return btnAddChost;
  }

  public JButton getBtnRchost() {
    return btnRchost;
  }

  public JButton getBtnRpHost() {
    return btnRpHost;
  }

  public JButton getBtnApHost() {
    return btnApHost;
  }
  
  
  
}
