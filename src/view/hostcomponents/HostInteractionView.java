package view.hostcomponents;

import java.awt.Font;
import javax.swing.JTabbedPane;
import model.database.PopulateMeets;

public class HostInteractionView extends JTabbedPane {
private PopulateMeets pop;
private HostListTab hlt;
private HostSwapTab hst;
  
  public HostInteractionView(PopulateMeets popTemp) {
    this.pop = popTemp;
    hlt = new HostListTab(pop);
    hst = new HostSwapTab(pop);
    
    addTab("Host List View", hlt);
    addTab("Host Swap View", hst);
    setFont(new Font("TimesRoman", Font.BOLD, 18));
  }

  public HostListTab getHlt() {
    return hlt;
  }

  public HostSwapTab getHst() {
    return hst;
  }

  public void setPop(PopulateMeets pop) {
    this.pop = pop;
  }
  
  
}
