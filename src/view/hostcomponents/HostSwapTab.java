package view.hostcomponents;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import model.database.PopulateMeets;
import model.meets.Meet;
import model.meets.School;
import view.mapcomponents.GeocoderMap;
import javax.swing.SwingConstants;

public class HostSwapTab extends JPanel {
  private GridBagLayout gridBagLayout;
  private JLabel lblMeettype;
  private GridBagConstraints gbcLblMeettype;
  private JComboBox meetTypeCb;
  private GridBagConstraints gbcMeetTypeCb;
  private JLabel lblMeetNumber;
  private GridBagConstraints gbcLblMeetNumber;
  private JComboBox meetNumbersCb;
  private GridBagConstraints gbcMeetNumbersCb;
  private JSeparator separator;
  GridBagConstraints gbcSeparator;
  private JLabel listHostLbl;
  private GridBagConstraints gbcListHostLbl;
  private JLabel lblSelectedMeet;
  private GridBagConstraints gbcLblSelectedMeet;
  private JList currentHostList;
  private GridBagConstraints gbcCurrentHostList;
  private JScrollPane scrollPane;
  private GridBagConstraints gbcScrollPane;
  private JList selectedMeetList;
  private JButton btnSwapSelected;
  private GridBagConstraints gbcBtnSwapSelected;
  private PopulateMeets pop;

  public HostSwapTab(PopulateMeets pop) {
    this.pop = pop;
    
    gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths =
        new int[] {0, 132, 300, 0, 300, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    gridBagLayout.rowHeights = new int[] {0, 0, 0, 0, 0, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    gridBagLayout.columnWeights = new double[] {0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    lblMeettype = new JLabel("Meet Type");
    lblMeettype.setFont(new Font("Times New Roman", Font.PLAIN, 20));
    gbcLblMeettype = new GridBagConstraints();
    gbcLblMeettype.insets = new Insets(0, 0, 5, 5);
    gbcLblMeettype.anchor = GridBagConstraints.EAST;
    gbcLblMeettype.gridx = 1;
    gbcLblMeettype.gridy = 1;
    add(lblMeettype, gbcLblMeettype);

    meetTypeCb = new JComboBox<Meet>();
    meetTypeCb.setFont(new Font("Times New Roman", Font.PLAIN, 20));
    gbcMeetTypeCb = new GridBagConstraints();
    gbcMeetTypeCb.insets = new Insets(0, 0, 5, 5);
    gbcMeetTypeCb.fill = GridBagConstraints.HORIZONTAL;
    gbcMeetTypeCb.gridx = 2;
    gbcMeetTypeCb.gridy = 1;
    add(meetTypeCb, gbcMeetTypeCb);

    meetTypeCb.addItem("Sectionals");
    meetTypeCb.addItem("Regionals");
    meetTypeCb.addItem("SemiStates");
    meetTypeCb.addItem("States");

    lblMeetNumber = new JLabel("Meet Number");
    lblMeetNumber.setFont(new Font("Times New Roman", Font.PLAIN, 20));
    gbcLblMeetNumber = new GridBagConstraints();
    gbcLblMeetNumber.insets = new Insets(0, 0, 5, 5);
    gbcLblMeetNumber.anchor = GridBagConstraints.EAST;
    gbcLblMeetNumber.gridx = 1;
    gbcLblMeetNumber.gridy = 3;
    add(lblMeetNumber, gbcLblMeetNumber);

    meetNumbersCb = new JComboBox();
    meetNumbersCb.setFont(new Font("Times New Roman", Font.PLAIN, 20));
    gbcMeetNumbersCb = new GridBagConstraints();
    gbcMeetNumbersCb.insets = new Insets(0, 0, 5, 5);
    gbcMeetNumbersCb.fill = GridBagConstraints.HORIZONTAL;
    gbcMeetNumbersCb.gridx = 2;
    gbcMeetNumbersCb.gridy = 3;
    add(meetNumbersCb, gbcMeetNumbersCb);

    separator = new JSeparator();
    gbcSeparator = new GridBagConstraints();
    gbcSeparator.gridwidth = 15;
    gbcSeparator.insets = new Insets(0, 0, 5, 0);
    gbcSeparator.gridx = 1;
    gbcSeparator.gridy = 5;
    add(separator, gbcSeparator);

    listHostLbl = new JLabel("Selected Meet's Current Host");
    listHostLbl.setFont(new Font("Times New Roman", Font.PLAIN, 20));
    gbcListHostLbl = new GridBagConstraints();
    gbcListHostLbl.insets = new Insets(0, 0, 5, 5);
    gbcListHostLbl.gridx = 2;
    gbcListHostLbl.gridy = 6;
    add(listHostLbl, gbcListHostLbl);

    lblSelectedMeet = new JLabel("Selected Meet's Potential Hosts");
    lblSelectedMeet.setFont(new Font("Times New Roman", Font.PLAIN, 20));
    gbcLblSelectedMeet = new GridBagConstraints();
    gbcLblSelectedMeet.anchor = GridBagConstraints.SOUTH;
    gbcLblSelectedMeet.insets = new Insets(0, 0, 5, 5);
    gbcLblSelectedMeet.gridx = 4;
    gbcLblSelectedMeet.gridy = 6;
    add(lblSelectedMeet, gbcLblSelectedMeet);

    currentHostList = new JList();
    currentHostList.setFont(new Font("Times New Roman", Font.PLAIN, 20));
    gbcCurrentHostList = new GridBagConstraints();
    gbcCurrentHostList.gridheight = 5;
    gbcCurrentHostList.insets = new Insets(0, 0, 5, 5);
    gbcCurrentHostList.fill = GridBagConstraints.BOTH;
    gbcCurrentHostList.gridx = 2;
    gbcCurrentHostList.gridy = 7;
    add(currentHostList, gbcCurrentHostList);

    scrollPane = new JScrollPane();
    gbcScrollPane = new GridBagConstraints();
    gbcScrollPane.gridheight = 5;
    gbcScrollPane.insets = new Insets(0, 0, 5, 5);
    gbcScrollPane.fill = GridBagConstraints.BOTH;
    gbcScrollPane.gridx = 4;
    gbcScrollPane.gridy = 7;
    add(scrollPane, gbcScrollPane);

    selectedMeetList = new JList();
    selectedMeetList.setFont(new Font("Times New Roman", Font.PLAIN, 20));
    scrollPane.setViewportView(selectedMeetList);

    btnSwapSelected = new JButton("Swap Selected");
    btnSwapSelected.setFont(new Font("Times New Roman", Font.PLAIN, 20));
    gbcBtnSwapSelected = new GridBagConstraints();
    gbcBtnSwapSelected.insets = new Insets(0, 0, 5, 5);
    gbcBtnSwapSelected.gridx = 3;
    gbcBtnSwapSelected.gridy = 8;
    add(btnSwapSelected, gbcBtnSwapSelected);

    this.meetTypeCb.setSelectedIndex(0);

    // setOpaque(true);
    // setBackground(Color.blue);
  }

  public void changeViewingParty() {
    int index = meetTypeCb.getSelectedIndex();
    if (index == -1) {
      return;
    }
    meetNumbersCb.removeAllItems();
    switch (index) {
      case 0: // Sectionals

        for (int i = 0; i < pop.getSectionalNum(); i++) {
          meetNumbersCb.addItem(pop.getSectionals(i));
        }
        this.meetNumbersCb.setSelectedIndex(0);
        break;
      case 1: // Regionals

        for (int i = 0; i < pop.getRegionalNum(); i++) {
          meetNumbersCb.addItem(pop.getRegionals(i));
        }
        this.meetNumbersCb.setSelectedIndex(0);
        break;
      case 2: // SemiStates

        for (int i = 0; i < pop.getSemistateNum(); i++) {
          meetNumbersCb.addItem(pop.getSemistates(i));
        }
        this.meetNumbersCb.setSelectedIndex(0);
        break;
      case 3: // States

        for (int i = 0; i < pop.getStateNum(); i++) {
          meetNumbersCb.addItem(pop.getStates(i));
        }
        this.meetNumbersCb.setSelectedIndex(0);
        break;
      default:
        break;
    }
  }


  public void updateLists() {
    Meet selected = (Meet) meetNumbersCb.getSelectedItem();
    if (selected == null) {
      return;
    }
    ArrayList<String> schools = new ArrayList<String>();
    ArrayList<String> host = new ArrayList<String>();
    host.add(selected.getHost());

    for (int i = 0; i < selected.getParticipants().size(); i++) {
      if (!selected.getParticipants().get(i).getName().equals(host.get(0))) {
        schools.add(selected.getParticipants().get(i).getName());
      }
    }
    selectedMeetList.setListData(schools.toArray());
    currentHostList.setListData(host.toArray());

    currentHostList.setSelectedIndex(0);
  }

  public void swapHost() {
    Meet selected = (Meet) meetNumbersCb.getSelectedItem();
    String tempA = (String) currentHostList.getSelectedValue(); // Previous host
    String tempB = (String) selectedMeetList.getSelectedValue(); // Users choice of new Host

    if (tempA == (null) || tempB == (null) || selected == (null)) {
      /*JOptionPane.showMessageDialog(null,
          "You must select a school to be swapped with the current host!", "Invalid Operation",
          JOptionPane.WARNING_MESSAGE);
          */
      return;
    }

    ArrayList<String> schools = new ArrayList<String>();
    ArrayList<String> host = new ArrayList<String>();
    host.add(tempB);

    for (int i = 0; i < selected.getParticipants().size(); i++) {
      if (!selected.getParticipants().get(i).getName().equals(host.get(0))) {
        schools.add(selected.getParticipants().get(i).getName());
      }
    }

    selectedMeetList.setListData(schools.toArray());
    currentHostList.setListData(host.toArray());
   
    currentHostList.setSelectedIndex(0);
    validate();
  }

  public JComboBox getMeetTypeCb() {
    return meetTypeCb;
  }

  public JComboBox getMeetNumbersCb() {
    return meetNumbersCb;
  }

  public JButton getBtnSwapSelected() {
    return btnSwapSelected;
  }

  public void updatePop(PopulateMeets pop) {
    this.pop = pop;
    int tempIndex= meetNumbersCb.getSelectedIndex();
    changeViewingParty();
    this.meetNumbersCb.setSelectedIndex(tempIndex);
    validate();
  }

  public JList getCurrentHostList() {
    return currentHostList;
  }

  public JList getSelectedMeetList() {
    return selectedMeetList;
  }
  
  
}
