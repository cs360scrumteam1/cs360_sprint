package view.mapcomponents;


import java.awt.Dimension;
import java.math.BigDecimal;
import java.util.ArrayList;
import javax.swing.JCheckBox;
import com.teamdev.jxmaps.Animation;
import com.teamdev.jxmaps.InfoWindow;
import com.teamdev.jxmaps.LatLng;
import com.teamdev.jxmaps.Map;
import com.teamdev.jxmaps.MapEvent;
import com.teamdev.jxmaps.MapMouseEvent;
import com.teamdev.jxmaps.MapReadyHandler;
import com.teamdev.jxmaps.MapStatus;
import com.teamdev.jxmaps.Marker;
import com.teamdev.jxmaps.MouseEvent;
import com.teamdev.jxmaps.swing.MapView;
import model.database.PopulateMeets;
import model.meets.School;

public class GeocoderMap extends MapView {
    // Setting of a ready handler to MapView object. onMapReady will be called when map initialization is done and
    // the map object is ready to use. Current implementation of onMapReady customizes the map object
 /*  private OptionsWindow optionsWindow;*/

  private ArrayList<Marker> mMarkers;
  private int temp;
  private double[] distanceArr = null;
  
    public GeocoderMap(PopulateMeets pop){
        // Setting of a ready handler to MapView object. onMapReady will be called when
        // map initialization is done and
        // the map object is ready to use. Current implementation of onMapReady
        // customizes the map object.
        setOnMapReadyHandler(new MapReadyHandler() {
            @Override
            public void onMapReady(MapStatus status) {
               if (status == MapStatus.MAP_STATUS_OK) {
                final Map map = getMap();// Getting the associated map object

                map.setCenter(new LatLng(41.117415, -85.107857));// Setting the map center
                map.setZoom(9.0);// Setting initial zoom value
                map.getCenter();// Getting the map center
                // Creating a map options object
                setMapSize(new Dimension(750, 750));      
                
                //validate
                validate();
               }
            }
        });


    }
    
    public void SectionalMarker(ArrayList<School> schools, String title) throws InterruptedException{
      boolean isHost;
      String level;
      String address;
      String name;
      BigDecimal lati;
      BigDecimal lngi;
      realDistance(schools);
      temp+= 1;
      if(temp > 1) {
      // Removing marker from the map
    //    clearMarkers();
        
        Map map = getMap();
        ArrayList<Marker> mMarkers = new ArrayList<Marker>();
        Marker marker = new Marker(map);

      }

           for( int j = 0; j < schools.size(); j++){
               name = schools.get(j).getName() +" High School, Indiana";
               address = schools.get(j).getAddress();
               level= schools.get(j).getCompLevel();
               isHost = schools.get(j).isHost();
               lati = schools.get(j).getLatitude();
               Double lat = lati.doubleValue();
               lngi = schools.get(j).getLongitude();
               Double lng = lngi.doubleValue();
               //System.out.println(name); 
               double distance = distanceArr[j];
               double avgD = getAverDistance(schools);
               double maxD = getMaximumDistance(schools);
               performGeocode(schools, name, address, level, isHost, title, distance, avgD, maxD, lat, lng);
               //String school, String level, boolean host, String title, float distance, Float avgDistance, Float maxDistance
               //Thread.sleep(1);
      }
}


  

    public double distance(double lat1, double lon1, double lat2, double lon2) {
          double a = 6378137, b = 6356752.314245, f = 1 / 298.257223563;
          double L = Math.toRadians(lon2 - lon1);
          double U1 = Math.atan((1 - f) * Math.tan(Math.toRadians(lat1)));
          double U2 = Math.atan((1 - f) * Math.tan(Math.toRadians(lat2)));
          double sinU1 = Math.sin(U1), cosU1 = Math.cos(U1);
          double sinU2 = Math.sin(U2), cosU2 = Math.cos(U2);
          double cosSqAlpha;
          double sinSigma;
          double cos2SigmaM;
          double cosSigma;
          double sigma;

          double lambda = L, lambdaP, iterLimit = 100;
          do 
          {
              double sinLambda = Math.sin(lambda), cosLambda = Math.cos(lambda);
              sinSigma = Math.sqrt(   (cosU2 * sinLambda)
                                      * (cosU2 * sinLambda)
                                      + (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda)
                                      * (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda)
                                  );
              if (sinSigma == 0) 
              {
                  return 0;
              }

              cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cosLambda;
              sigma = Math.atan2(sinSigma, cosSigma);
              double sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
              cosSqAlpha = 1 - sinAlpha * sinAlpha;
              cos2SigmaM = cosSigma - 2 * sinU1 * sinU2 / cosSqAlpha;

              double C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
              lambdaP = lambda;
              lambda =    L + (1 - C) * f * sinAlpha  
                          *   (sigma + C * sinSigma   
                                  *   (cos2SigmaM + C * cosSigma
                                          *   (-1 + 2 * cos2SigmaM * cos2SigmaM)
                                      )
                              );
          
          } while (Math.abs(lambda - lambdaP) > 1e-12 && --iterLimit > 0);

          if (iterLimit == 0) 
          {
              return 0;
          }

          double uSq = cosSqAlpha * (a * a - b * b) / (b * b);
          double A = 1 + uSq / 16384
                  * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
          double B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));
          double deltaSigma = 
                      B * sinSigma
                          * (cos2SigmaM + B / 4
                              * (cosSigma 
                                  * (-1 + 2 * cos2SigmaM * cos2SigmaM) - B / 6 * cos2SigmaM
                                      * (-3 + 4 * sinSigma * sinSigma)
                                          * (-3 + 4 * cos2SigmaM * cos2SigmaM)));
          
          double s = b * A * (sigma - deltaSigma);
          
          return s/1609.344;
      }

    private double[] realDistance(ArrayList<School> schools) {
      boolean isHost;
      BigDecimal lat;
      BigDecimal lng;
      double distanceToHost;
      distanceArr= new double[schools.size()];
      
           for( int j = 0; j < schools.size(); j++){
             isHost = schools.get(j).isHost();
             if (isHost) {
              lat = schools.get(j).getLatitude();
              Double lat1 = lat.doubleValue();
              lng = schools.get(j).getLongitude();
              Double lng1 = lng.doubleValue();
                for( int i = 0; i < schools.size(); i++){
                   lat = schools.get(i).getLatitude();
                   Double lat2 = lat.doubleValue();
                   lng = schools.get(i).getLongitude();
                   Double lng2 = lng.doubleValue();
                   distanceToHost = Double.parseDouble(String.format("%.2f",distance(lat1, lng1, lat2, lng2)));
                   distanceArr[i]= distanceToHost;      
                 }
             }     
           }
           return distanceArr;
    }
  
    public double getMaximumDistance(ArrayList<School> schools) {
    double longestDistance = 0;

    realDistance(schools);

          for (int k = 0; k < distanceArr.length; k++){
              double distance = distanceArr[k];
              if (distance > longestDistance){
                  longestDistance = distance;

              } 
          }
          return longestDistance;
    }
    
    public double getAverDistance(ArrayList<School> schools) {
    double DistanceSum = 0;
    double DistanceAve = 0;
   
    realDistance(schools);

    for (int k = 0; k < distanceArr.length; k++){
        DistanceSum += distanceArr[k];
        }
    DistanceAve = DistanceSum/distanceArr.length;
    return Double.parseDouble(String.format("%.3f",DistanceAve));
    }

 
    @Override
    public void removeNotify() {
        super.removeNotify();
       // optionsWindow.dispose();
    }


    public void removeMarkers() {
        for (Marker marker: mMarkers) {
            marker.remove();
        }
        mMarkers.clear();
    }
    
    
 
    public void clearMarkers(){

      for (Marker marker : mMarkers) {
     //   final Map map = getMap();  
        marker.setVisible(false);
        marker.remove();
         // marker.getVisible();
      }
      mMarkers = null;
      
      
    }
    



    private void performGeocode(ArrayList<School> schools, String school, String address, String level, boolean host, String title, double distance, double avgDistance, double maxDistance, double lat, double lng) {
        // Getting the associated map object
      
      
        final Map map = getMap();

        // Creating markers for each place     
        Marker marker = new Marker(map);
       
      // map.actionTrigger(map.doClick(););
                    if (host) {
                    //latitude and longitude for distance

                    //Set marker icon for host
                    marker.setIcon("https://www.habsmonmouth.org/girlsprep/wp-content/uploads/sites/5/2015/08/icon-map-marker-pink-h.png");
                    
                    marker.setAnimation(Animation.BOUNCE);
                    map.setCenter(new LatLng(lat,lng));
                    // Setting marker position
                    marker.setPosition(map.getCenter());
                    }
                    else {   
                      //latitude and longitude for distance

                    //Set marker icon for the participants
                      String icon = "";
                      switch (title) {
                        case "Sectional 1": case "Sectional 2": case "Sectional 3": case "Sectional 4": case "Sectional 5":
                        case "Sectional 6": case "Sectional 7": case "Sectional 8": case "Sectional 9": case "Sectional 10":
                        case "Sectional 11": case "Sectional 12": case "Sectional 13": case "Sectional 14":  case "Sectional 15":
                        case "Sectional 16": case "Sectional 17": case "Sectional 18": case "Sectional 19": case "Sectional 20":
                        case "Sectional 21": case "Sectional 22": case "Sectional 23": case "Sectional 24": case "Sectional 25":
                        case "Sectional 26": case "Sectional 27": case "Sectional 28": case "Sectional 29": case "Sectional 30":
                        case "Sectional 31": case "Sectional 32":
                            icon = "green"; 
                            break; 
                        case "Regional 1":  case "Regional 2": case "Regional 3": case "Regional 4": case "Regional 5": case "Regional 6":
                        case "Regional 7": case "Regional 8": case "Regional 9": case "Regional 10": case "Regional 11": case "Regional 12":
                        case "Regional 13": case "Regional 14": case "Regional 15": case "Regional 16":
                            icon = "blue";  
                            break; 
                        case "Semi-State 1":  case "Semi-State 2": case "Semi-State 3": case "Semi-State 4": 
                            icon = "yellow";  
                            break;
                        case "State 1": 
                            icon = "red";  
                            break;
                    }  
                      icon = "http://maps.google.com/mapfiles/ms/icons/"+ icon +".png";
                      marker.setIcon(icon);
                    }
                    
                    // Setting position of the marker to the result location
                    marker.setPosition(new LatLng(lat,lng));
                    //marker.setPosition(mouseEvent.latLng());
                    

                    marker.setClickable(true);// isEnabled()
                    marker.getClickable();
                    
                    //public void DeleteMarkers(){
                    //Delete all marker when user mouse right click on the map 
                    map.addEventListener("rightclick", new MapMouseEvent() {
                      @Override
                      public void onEvent(MouseEvent mouseEvent) {
                          // Removing marker from the map
                         marker.remove();
                      }
                    });
                   
                     // Creating an information window
                    InfoWindow infoWindow = new InfoWindow(map);
                         //mouseover marker             
                            marker.addEventListener("mouseover", new MapEvent() {
                            
                              @Override
                              public void onEvent() {
                                    
                                if(host) {             
                                  // Putting the address and location to the content of the information window
                                  infoWindow.setContent("<b>"+"<h1 style=\"color:#FF00FF\">" + "Host School"
                                  + "</h1>"+ "<h3 style=\"color:#02B76F\">" +school+  "</h3>"
                                      + "<br>"+ title  + "</b>" );
                                }
                                else {                      
                                  // Putting the address and location to the content of the information window
                                  infoWindow.setContent("<b>"+"<h3 style=\"color:#02B76F\">" +school+  "</h3>"+ "<br>"
                                  + title + "</b>" );
                                    }
                                // Moving the information window to the result location
                                infoWindow.setPosition(new LatLng(lat,lng));
                                // Showing of the information window
                                infoWindow.open(map, marker); 
                              }
                          });

                            //mouseout marker 
                          marker.addEventListener("mouseout", new MapEvent() {
                              @Override
                              public void onEvent() {                           
                                 infoWindow.close();
                              }
                          });
                          marker.addEventListener("click", new MapEvent() {
                              @Override
                              public void onEvent() {
                                // Creating an information window
                                    InfoWindow infoWindow = new InfoWindow(map);
                                    // Putting the address and location to the content of the information window
                                    if(host) {              
                                      infoWindow.setContent("<b>"
                                          +"<h1 style=\"color:#FF00FF\">" + "Host School"
                                          + "</h1>"
                                          +"<h2 style=\"color:#02B79F\">" + school
                                          +  "</h2> "+ title  
                                          +"</b><br> <b> Address: " + address
                                          +"</b><br> <b> Average Travel Distances: </b>" + avgDistance + "Miles" 
                                          +"<br> <b> Farthest Travel Distances: </b>" + maxDistance + "Miles"
                                          + "<h4 style=\"color:#F37A00\"> Level: "+ level + "</h6><br> Latitude , Longitude :"+ new LatLng(lat,lng).toString());

                                   }
                                            else { 
                                              infoWindow.setContent("<b>"+"<h3 style=\"color:#02B76F\">" + school  
                                                  + "</h3>"+ title + "</b><br> <b> Address: " + address
                                                  + "<br><b>Distance to the host school: </b></br>"+ distance + "miles "
                                                  + "<h4 style=\"color:#F37A00\"> Level: "+ level + "</h4>"
                                                  + "<br> Latitude , Longitude: "+ new LatLng(lat,lng).toString());
                                                }
                                    // Moving the information window to the result location
                                    infoWindow.setPosition(new LatLng(lat,lng));
                                    // Showing of the information window
                                    infoWindow.open(map, marker);      
                              }
                              
                              
                    });


                }

            }
    

