package view.mapcomponents;

import javax.swing.JPanel;
import model.database.PopulateMeets;
import model.meets.Meet;
import model.meets.School;
import java.awt.GridBagLayout;
import javax.swing.JComboBox;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class MapOptionsView extends JPanel {
  private GridBagLayout gridBagLayout;
  private JComboBox cbSelectionType;
  private JComboBox cbViewingParty;
  private GridBagConstraints gbcA;
  private GridBagConstraints gbcB;
  private GridBagConstraints gbcC;
  private GridBagConstraints gbcD;
  private GridBagConstraints gbcE;
  private JLabel lblParticipantType;
  private JLabel lblParticipants;
  private PopulateMeets pop;
  private JScrollPane tableSroll;
  private JTable table;
  private final String[] colNames =
      {"School Name ", "Distance to Host", "Average Distance", "Max Distance"};

  public MapOptionsView(PopulateMeets pop) {
    this.pop = pop;
    gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[] {0, 0, 0};
    gridBagLayout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    gridBagLayout.columnWeights = new double[] {1.0, 1.0, Double.MIN_VALUE};
    gridBagLayout.rowWeights =
        new double[] {1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    lblParticipantType = new JLabel("Participant Type ");
    lblParticipantType.setFont(new Font("Times New Roman", Font.PLAIN, 20));
    gbcC = new GridBagConstraints();
    gbcC.insets = new Insets(0, 0, 5, 5);
    gbcC.anchor = GridBagConstraints.EAST;
    gbcC.gridx = 0;
    gbcC.gridy = 0;
    add(lblParticipantType, gbcC);

    cbSelectionType = new JComboBox<String>();
    cbSelectionType.setFont(new Font("Times New Roman", Font.PLAIN, 20));
    gbcA = new GridBagConstraints();
    gbcA.insets = new Insets(0, 0, 5, 0);
    gbcA.fill = GridBagConstraints.HORIZONTAL;
    gbcA.gridx = 1;
    gbcA.gridy = 0;
    add(cbSelectionType, gbcA);

    cbSelectionType.addItem("Sectionals");
    cbSelectionType.addItem("Regionals");
    cbSelectionType.addItem("SemiStates");
    cbSelectionType.addItem("States");

    lblParticipants = new JLabel("Participants");
    lblParticipants.setFont(new Font("Times New Roman", Font.PLAIN, 20));
    gbcD = new GridBagConstraints();
    gbcD.insets = new Insets(0, 0, 5, 5);
    gbcD.anchor = GridBagConstraints.EAST;
    gbcD.gridx = 0;
    gbcD.gridy = 1;
    add(lblParticipants, gbcD);

    cbViewingParty = new JComboBox<Meet>();
    cbViewingParty.setFont(new Font("Times New Roman", Font.PLAIN, 20));
    gbcB = new GridBagConstraints();
    gbcB.insets = new Insets(0, 0, 5, 0);
    gbcB.fill = GridBagConstraints.HORIZONTAL;
    gbcB.gridx = 1;
    gbcB.gridy = 1;
    add(cbViewingParty, gbcB);

    tableSroll = new JScrollPane();
    gbcE = new GridBagConstraints();
    gbcE.gridheight = 6;
    gbcE.gridwidth = 2;
    gbcE.fill = GridBagConstraints.BOTH;
    gbcE.gridx = 0;
    gbcE.gridy = 3;
    add(tableSroll, gbcE);

    table = new JTable();
    table.setFont(new Font("Times New Roman", Font.PLAIN, 20));
    tableSroll.setViewportView(table);

  }

  public void changeViewingParty() {
    int index = cbSelectionType.getSelectedIndex();
    if (index == -1) {
      return;
    }
    cbViewingParty.removeAllItems();
    switch (index) {
      case 0: // Sectionals

        for (int i = 0; i < pop.getSectionalNum(); i++) {
          cbViewingParty.addItem(pop.getSectionals(i));
        }

        break;
      case 1: // Regionals

        for (int i = 0; i < pop.getRegionalNum(); i++) {
          cbViewingParty.addItem(pop.getRegionals(i));
        }

        break;
      case 2: // SemiStates

        for (int i = 0; i < pop.getSemistateNum(); i++) {
          cbViewingParty.addItem(pop.getSemistates(i));
        }

        break;
      case 3: // States

        for (int i = 0; i < pop.getStateNum(); i++) {
          cbViewingParty.addItem(pop.getStates(i));
        }

        break;
      default:
        break;
    }
  }

  public void updateMap(GeocoderMap map) {
    Meet selected = (Meet) cbViewingParty.getSelectedItem();
    if (selected == null) {
      return;
    }
    ArrayList<School> schools = selected.getParticipants();
    String[][] info = new String[schools.size()][4];
    boolean isHost;
    BigDecimal lat;
    BigDecimal lng;
    double distanceToHost=0.0;
    double[] distanceArr= new double[schools.size()];
    int hostNum = 0;

    for (int k = 0; k < schools.size(); k++) {
      info[k][0] = schools.get(k).getName();
      for( int i = 0; i < schools.size(); i++) {
      isHost = schools.get(i).isHost();
      if (isHost) {
         lat = schools.get(i).getLatitude();
         Double lat1 = lat.doubleValue();
         lng = schools.get(i).getLongitude();
         Double lng1 = lng.doubleValue();
             hostNum = i;
         for( int j = 0; j < schools.size(); j++){
            lat = schools.get(j).getLatitude();
            Double lat2 = lat.doubleValue();
            lng = schools.get(j).getLongitude();
            Double lng2 = lng.doubleValue();
            distanceToHost = Double.parseDouble(String.format("%.2f",map.distance(lat1, lng1, lat2, lng2)));
            distanceArr[j]= distanceToHost;
         }
      }
      info[k][1] = distanceArr[k]+ " Miles";
      }
    }
    info[hostNum][2] = map.getAverDistance(schools) + " Miles";
    info[hostNum][3] = map.getMaximumDistance(schools) + " Miles";

    table = new JTable(info, colNames) {
      @Override
      public boolean isCellEditable(int row, int column) {
        return false;
      }
    };
    table.setFont(new Font("TimesRoman", Font.PLAIN, 18));
    for (int k = 0; k < 4; k++) {
      table.getColumnModel().getColumn(k).setPreferredWidth(125);
    }
    table.setRowHeight(30);
    table.getTableHeader().setFont(new Font("TimesRoman", Font.PLAIN, 18));
    tableSroll.setViewportView(table);
    tableSroll.validate();

    try {
      map.SectionalMarker(selected.getParticipants(), selected.getTitle());
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
    
  

  public JComboBox getCbSelectionType() {
    return cbSelectionType;
  }

  public JComboBox getCbViewingParty() {
    return cbViewingParty;
  }

  public void updatePop(PopulateMeets pop) {
    this.pop = pop;
    validate();
  }
  
}
