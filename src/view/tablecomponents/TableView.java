package view.tablecomponents;

import java.awt.Font;
import java.util.ArrayList;
import javax.swing.JTabbedPane;
import model.database.PopulateMeets;
import model.meets.Meet;

public class TableView extends JTabbedPane {
  private JTabbedPane sectionTabs;
  private JTabbedPane regionTabs;
  private JTabbedPane semiTabs;
  private JTabbedPane stateTabs;
  private ArrayList<Table> sectionalTables;
  private ArrayList<Table> regionalTables;
  private ArrayList<Table> semistateTables;
  private ArrayList<Table> stateTables;
  private Meets meets;
  private PopulateMeets pop;

  private enum Meets {
    Sectional, Regional, SemiState, State
  }

  public TableView(PopulateMeets pop) {
    this.pop = pop;
    // initializes the JTabbedPanel
    sectionTabs = new JTabbedPane();
    regionTabs = new JTabbedPane();
    semiTabs = new JTabbedPane();
    stateTabs = new JTabbedPane();

    // Set the JTabbedPanels policy to Scroll Tab Layout
    sectionTabs.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
    regionTabs.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
    semiTabs.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
    stateTabs.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);

    // Sets the TabbedPanes font size
    sectionTabs.setFont(new Font("TimesRoman", Font.BOLD, 18));
    regionTabs.setFont(new Font("TimesRoman", Font.BOLD, 18));
    semiTabs.setFont(new Font("TimesRoman", Font.BOLD, 18));
    stateTabs.setFont(new Font("TimesRoman", Font.BOLD, 18));

    // initializes the ArrayLists of Tables
    sectionalTables = new ArrayList<Table>();
    regionalTables = new ArrayList<Table>();
    semistateTables = new ArrayList<Table>();
    stateTables = new ArrayList<Table>();

    // Populates the Tables with the given information.
    initTables(meets.Sectional, sectionalTables, sectionTabs);
    initTables(meets.Regional, regionalTables, regionTabs);
    initTables(meets.SemiState, semistateTables, semiTabs);
    initTables(meets.State, stateTables, stateTabs);

    // Adds the populated JTabbedPanes to the main JTabbedPane.
    addTab("Sectional Meets", sectionTabs);
    addTab("Regional Meets", regionTabs);
    addTab("SemiState Meets", semiTabs);
    addTab("State Meets", stateTabs);
    setFont(new Font("TimesRoman", Font.BOLD, 18));
    setVisible(true);
  }

  public void initTables(Meets type, ArrayList<Table> lists, JTabbedPane tabPane) {
    Meet meet;
    switch (type) {
      case Sectional:
        for (int i = 0; i < pop.getSectionalNum(); i++) {
          meet = (pop.getSectionals(i));
          lists.add(new Table(meet));
          tabPane.addTab(meet.getTitle(), lists.get(i));
        }
        break;

      case Regional:
        for (int i = 0; i < pop.getRegionalNum(); i++) {
          meet = (pop.getRegionals(i));
          lists.add(new Table(meet));
          tabPane.addTab(meet.getTitle(), lists.get(i));
        }
        break;

      case SemiState:
        for (int i = 0; i < pop.getSemistateNum(); i++) {
          meet = (pop.getSemistates(i));
          lists.add(new Table(meet));
          tabPane.addTab(meet.getTitle(), lists.get(i));
        }
        break;

      case State:
        for (int i = 0; i < pop.getStateNum(); i++) {
          meet = (pop.getStates(i));
          lists.add(new Table(meet));
          tabPane.addTab(meet.getTitle(), lists.get(i));
        }
        break;
      default:
        break;

    }
  }

  public ArrayList<Table> getSectionals() {
    return this.sectionalTables;
  }

  public ArrayList<Table> getRegionals() {
    return this.regionalTables;
  }

  public ArrayList<Table> getSemiStates() {
    return this.semistateTables;
  }

  public ArrayList<Table> getStates() {
    return this.stateTables;
  }

  public void updatePop(PopulateMeets pop) {
    this.pop = pop;
    // initializes the JTabbedPanel
    sectionTabs = new JTabbedPane();
    regionTabs = new JTabbedPane();
    semiTabs = new JTabbedPane();
    stateTabs = new JTabbedPane();

    // Set the JTabbedPanels policy to Scroll Tab Layout
    sectionTabs.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
    regionTabs.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
    semiTabs.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
    stateTabs.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);

    // Sets the TabbedPanes font size
    sectionTabs.setFont(new Font("TimesRoman", Font.BOLD, 18));
    regionTabs.setFont(new Font("TimesRoman", Font.BOLD, 18));
    semiTabs.setFont(new Font("TimesRoman", Font.BOLD, 18));
    stateTabs.setFont(new Font("TimesRoman", Font.BOLD, 18));

    // initializes the ArrayLists of Tables
    sectionalTables = new ArrayList<Table>();
    regionalTables = new ArrayList<Table>();
    semistateTables = new ArrayList<Table>();
    stateTables = new ArrayList<Table>();
    
    //updates the tables with current information
    initTables(meets.Sectional, sectionalTables, sectionTabs);
    initTables(meets.Regional, regionalTables, regionTabs);
    initTables(meets.SemiState, semistateTables, semiTabs);
    initTables(meets.State, stateTables, stateTabs);
    validate();
  }
}