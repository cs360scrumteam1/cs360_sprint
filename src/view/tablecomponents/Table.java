package view.tablecomponents;

import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import model.meets.Meet;
import model.meets.School;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.BoxLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;

public class Table extends JPanel {
  private JTable table;
  private JScrollPane scroll;
  private final int columns = 7;
  private String[][] info;
  private int size;
  private JPanel container;
  private final String[] colNames = {"School Name ", "C-Level ", "Address ",
      "Host Status ", "Distance to Host", "AVG Distance", "MAX Distance"};
  private TableOptionsView tov;
  private String tempDistance;
  private GridBagConstraints gbcA;


  public Table(Meet meet) {
    
    size = meet.getParticipants().size();
    info = new String[size][columns];


    for (int j = 0; j < size; j++) {
      
      info[j][0] = meet.getParticipants().get(j).getName();
      info[j][1] = meet.getParticipants().get(j).getCompLevel();
      info[j][2] = meet.getParticipants().get(j).getAddress();

      if (meet.getParticipants().get(j).isHost()) {
        info[j][3] = "Host";
        info[j][5] = meet.getAverageDistance() + " Miles";
        info[j][6] = meet.getFarthestDistance() + " Miles";
      } else {
        info[j][3] = "";
      }
      tempDistance = meet.getParticipants().get(j).getDistanceToHost() + " Miles";
      info[j][4] = tempDistance;
    }

    setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
    container = new JPanel();
    gbcA = new GridBagConstraints();
    gbcA.anchor = GridBagConstraints.NORTHWEST;
    gbcA.gridx = 1;
    gbcA.gridy = 0;
    container.setLayout(new BoxLayout(container, BoxLayout.X_AXIS));
    table = new JTable(info, colNames) {
      @Override
      public boolean isCellEditable(int row, int column) {
        return false;
      }
    };
    table.setFont(new Font("TimesRoman", Font.PLAIN, 18));
    for (int k = 0; k < columns; k++) {
      table.getColumnModel().getColumn(k).setPreferredWidth(550);
      if (k == 1) {
        table.getColumnModel().getColumn(k).setPreferredWidth(200);
      } else if (k == 2) {
        table.getColumnModel().getColumn(k).setPreferredWidth(700);
      } else if (k == 3) {
        table.getColumnModel().getColumn(k).setPreferredWidth(300);
      } else if (k == 4) {
        table.getColumnModel().getColumn(k).setPreferredWidth(350);
      } else if (k == 5) {
        table.getColumnModel().getColumn(k).setPreferredWidth(300);
      } else if (k == 6) {
        table.getColumnModel().getColumn(k).setPreferredWidth(300);
      }

    }
    table.setRowHeight(30);
    table.getTableHeader().setFont(new Font("TimesRoman", Font.PLAIN, 18));
    scroll = new JScrollPane(table);
    scroll.setPreferredSize(new Dimension(1300, 800));
    container.add(scroll);
    tov = new TableOptionsView();
    container.add(tov);
    add(container);
    setVisible(true);
  }

  public TableOptionsView getTableOptionsView() {
    return tov;
  }
}
