package view.tablecomponents;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;


public class TableOptionsView extends JPanel {
    private JTextField txtSchoolName;
    private JTextField txtSchoolAddress; 
    private JTextField  txtCompLevel;
    private JTextField txtHostStatus;
    private JTextField txtNewName;
    private JTextField txtNewAddress;
    private JTextField txtNewCompLevel;
    private JTextField txtNewHostStatus;
    private JLabel lblSchoolName;
    private JLabel lblSchoolAddress;
    private JLabel lblSchoolCompetitiveLevel;
    private JLabel lblSchoolHostStatus;
    private JLabel lblNewSchoolName;
    private JLabel lblNewSchoolAddress;
    private JLabel lblNewSchoolCompetitive;
    private JLabel lblNewSchoolHost;
    private JButton btnEditSchoolInfo;
    private JButton btnSaveEditedInformation;
    private JButton btnAddSchool;
    private JPanel selectedPanel;
    private JPanel newSchoolPanel;
    private GridBagLayout gblA;
    private GridBagLayout gblB;
    private GridBagConstraints gcbA;
    private GridBagConstraints gcbB;
    private GridBagConstraints gcbC;
    private GridBagConstraints gcbD;
    private GridBagConstraints gcbE;
    private GridBagConstraints gcbF;
    private GridBagConstraints gcbG;
    private GridBagConstraints gcbH;
    private GridBagConstraints gcbI;
    private GridBagConstraints gcbJ;
    private GridBagConstraints gcbK;
    private GridBagConstraints gcbL;
    private GridBagConstraints gcbM;
    private GridBagConstraints gcbN;
    private GridBagConstraints gcbO;
    private GridBagConstraints gcbP;
    private GridBagConstraints gcbQ;
    private GridBagConstraints gcbR;
    private GridBagConstraints gcbS;
    
    public TableOptionsView() {
        
        //Sets the Layout of the Primary JPanel that will be compose of two others JPanels.
        setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Options", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        ((TitledBorder) getBorder()).setTitleFont(new Font("TimesRoman", Font.PLAIN, 18));

        selectedPanel = new JPanel();
        selectedPanel.setBorder(new TitledBorder(null, "Selected School Information", TitledBorder.CENTER, TitledBorder.TOP, null, null));
        ((TitledBorder) selectedPanel.getBorder()).setTitleFont(new Font("TimesRoman", Font.PLAIN, 18));
        add(selectedPanel);
        
        gblA = new GridBagLayout();
        gblA.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
        gblA.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        gblA.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
        gblA.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        selectedPanel.setLayout(gblA);
        
        lblSchoolName = new JLabel("School Name");
        lblSchoolName.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        gcbA = new GridBagConstraints();
        gcbA.insets = new Insets(0, 0, 5, 5);
        gcbA.gridx = 1;
        gcbA.gridy = 1;
        selectedPanel.add(lblSchoolName, gcbA);
        
        txtSchoolName = new JTextField();
        txtSchoolName.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        gcbB = new GridBagConstraints();
        gcbB.insets = new Insets(0, 0, 5, 5);
        gcbB.fill = GridBagConstraints.HORIZONTAL;
        gcbB.gridx = 3;
        gcbB.gridy = 1;
        selectedPanel.add(txtSchoolName, gcbB);
        txtSchoolName.setColumns(10);
        
        lblSchoolAddress = new JLabel("School Address");
        lblSchoolAddress.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        gcbC = new GridBagConstraints();
        gcbC.insets = new Insets(0, 0, 5, 5);
        gcbC.gridx = 1;
        gcbC.gridy = 3;
        selectedPanel.add(lblSchoolAddress, gcbC);
        
        txtSchoolAddress = new JTextField();
        txtSchoolAddress.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        gcbD = new GridBagConstraints();
        gcbD.anchor = GridBagConstraints.NORTH;
        gcbD.insets = new Insets(0, 0, 5, 5);
        gcbD.fill = GridBagConstraints.HORIZONTAL;
        gcbD.gridx = 3;
        gcbD.gridy = 3;
        selectedPanel.add(txtSchoolAddress, gcbD);
        txtSchoolAddress.setColumns(10);
        
        lblSchoolCompetitiveLevel = new JLabel("School Competitive Level");
        lblSchoolCompetitiveLevel.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        gcbE = new GridBagConstraints();
        gcbE.insets = new Insets(0, 0, 5, 5);
        gcbE.gridx = 1;
        gcbE.gridy = 5;
        selectedPanel.add(lblSchoolCompetitiveLevel, gcbE);
        
        txtCompLevel = new JTextField();
        txtCompLevel.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        gcbF = new GridBagConstraints();
        gcbF.anchor = GridBagConstraints.NORTH;
        gcbF.insets = new Insets(0, 0, 5, 5);
        gcbF.fill = GridBagConstraints.HORIZONTAL;
        gcbF.gridx = 3;
        gcbF.gridy = 5;
        selectedPanel.add(txtCompLevel, gcbF);
        txtCompLevel.setColumns(10);
        
        lblSchoolHostStatus = new JLabel("School Host Status");
        lblSchoolHostStatus.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        gcbG = new GridBagConstraints();
        gcbG.insets = new Insets(0, 0, 5, 5);
        gcbG.gridx = 1;
        gcbG.gridy = 7;
        selectedPanel.add(lblSchoolHostStatus, gcbG);
        
        txtHostStatus = new JTextField();
        txtHostStatus.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        gcbH = new GridBagConstraints();
        gcbH.anchor = GridBagConstraints.NORTH;
        gcbH.insets = new Insets(0, 0, 5, 5);
        gcbH.fill = GridBagConstraints.HORIZONTAL;
        gcbH.gridx = 3;
        gcbH.gridy = 7;
        selectedPanel.add(txtHostStatus, gcbH);
        txtHostStatus.setColumns(10);
        
        btnEditSchoolInfo = new JButton("Edit School Information");
        btnEditSchoolInfo.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        gcbI = new GridBagConstraints();
        gcbI.insets = new Insets(0, 0, 5, 5);
        gcbI.gridx = 1;
        gcbI.gridy = 9;
        selectedPanel.add(btnEditSchoolInfo, gcbI);
        
        btnSaveEditedInformation = new JButton("Save Edited Information");
        btnSaveEditedInformation.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        gcbJ = new GridBagConstraints();
        gcbJ.insets = new Insets(0, 0, 5, 5);
        gcbJ.gridx = 3;
        gcbJ.gridy = 9;
        selectedPanel.add(btnSaveEditedInformation, gcbJ);
        
        newSchoolPanel = new JPanel();
        newSchoolPanel.setBorder(new TitledBorder(null, "Add New School", TitledBorder.LEADING, TitledBorder.TOP, null, null));
        ((TitledBorder) newSchoolPanel.getBorder()).setTitleFont(new Font("TimesRoman", Font.PLAIN, 18));
        add(newSchoolPanel);
        
        gblB = new GridBagLayout();
        gblB.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
        gblB.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        gblB.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
        gblB.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        newSchoolPanel.setLayout(gblB);
        
        lblNewSchoolName = new JLabel("New School Name");
        lblNewSchoolName.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        gcbK = new GridBagConstraints();
        gcbK.insets = new Insets(0, 0, 5, 5);
        gcbK.gridx = 1;
        gcbK.gridy = 0;
        newSchoolPanel.add(lblNewSchoolName, gcbK);
        
        txtNewName = new JTextField();
        txtNewName.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        gcbL = new GridBagConstraints();
        gcbL.insets = new Insets(0, 0, 5, 5);
        gcbL.fill = GridBagConstraints.HORIZONTAL;
        gcbL.gridx = 3;
        gcbL.gridy = 0;
        newSchoolPanel.add(txtNewName, gcbL);
        txtNewName.setColumns(10);
        
        lblNewSchoolAddress = new JLabel("New School Address");
        lblNewSchoolAddress.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        gcbM = new GridBagConstraints();
        gcbM.insets = new Insets(0, 0, 5, 5);
        gcbM.gridx = 1;
        gcbM.gridy = 2;
        newSchoolPanel.add(lblNewSchoolAddress, gcbM);
        
        txtNewAddress = new JTextField();
        txtNewAddress.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        gcbN = new GridBagConstraints();
        gcbN.insets = new Insets(0, 0, 5, 5);
        gcbN.fill = GridBagConstraints.HORIZONTAL;
        gcbN.gridx = 3;
        gcbN.gridy = 2;
        newSchoolPanel.add(txtNewAddress, gcbN);
        txtNewAddress.setColumns(10);
        
        lblNewSchoolCompetitive = new JLabel("New School Competitive Level");
        lblNewSchoolCompetitive.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        gcbO = new GridBagConstraints();
        gcbO.insets = new Insets(0, 0, 5, 5);
        gcbO.gridx = 1;
        gcbO.gridy = 4;
        newSchoolPanel.add(lblNewSchoolCompetitive, gcbO);
        
        txtNewCompLevel = new JTextField();
        txtNewCompLevel.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        gcbP = new GridBagConstraints();
        gcbP.insets = new Insets(0, 0, 5, 5);
        gcbP.fill = GridBagConstraints.HORIZONTAL;
        gcbP.gridx = 3;
        gcbP.gridy = 4;
        newSchoolPanel.add(txtNewCompLevel, gcbP);
        txtNewCompLevel.setColumns(10);
        
        lblNewSchoolHost = new JLabel("New School Host Status");
        lblNewSchoolHost.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        gcbQ = new GridBagConstraints();
        gcbQ.insets = new Insets(0, 0, 5, 5);
        gcbQ.gridx = 1;
        gcbQ.gridy = 6;
        newSchoolPanel.add(lblNewSchoolHost, gcbQ);
        
        txtNewHostStatus = new JTextField();
        txtNewHostStatus.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        gcbR = new GridBagConstraints();
        gcbR.anchor = GridBagConstraints.NORTH;
        gcbR.insets = new Insets(0, 0, 5, 5);
        gcbR.fill = GridBagConstraints.HORIZONTAL;
        gcbR.gridx = 3;
        gcbR.gridy = 6;
        newSchoolPanel.add(txtNewHostStatus, gcbR);
        txtNewHostStatus.setColumns(10);
        
        btnAddSchool = new JButton("Add school");
        btnAddSchool.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        gcbS = new GridBagConstraints();
        gcbS.insets = new Insets(0, 0, 0, 5);
        gcbS.gridx = 1;
        gcbS.gridy = 8;
        newSchoolPanel.add(btnAddSchool, gcbS);
        
    }

    public JButton getBtnEditSchoolInfo() {
        return btnEditSchoolInfo;
    }

    public JButton getBtnSaveEditedInformation() {
        return btnSaveEditedInformation;
    }

    public JButton getBtnAddSchool() {
        return btnAddSchool;
    }

    public JPanel getSelectedPanel() {
        return selectedPanel;
    }

    public JPanel getNewSchoolPanel() {
        return newSchoolPanel;
    }
    
}
