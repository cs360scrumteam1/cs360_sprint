package view;

import com.teamdev.jxmaps.LatLng;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;
import view.hostcomponents.HostInteractionView;
import view.hostcomponents.HostSwapTab;
import view.mapcomponents.GeocoderMap;
import view.mapcomponents.MapOptionsView;
import view.tablecomponents.TableView;
import java.awt.Color;
import java.awt.Font;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.util.ArrayList;
import controller.Listener;
import model.database.PopulateMeets;
import model.meets.Meet;
import model.meets.School;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MainView extends JFrame {
  private JPanel tabPanel;
  private JButton[] tabs;
  private Listener listener;
  private TableView tableView;
  private GraphicsDevice gd;
  private int width;
  private int height;
  private GeocoderMap mapView;
  private MapOptionsView mapOptView;
  private HostInteractionView hp;
  // Persistant positioning of the large and small winows that will occupy the frame.
  private int largeX;
  private int largeY;
  private int smallX;
  private int smallY;
  private JMenuBar menuBar;
  private JMenu fileMenu;
  private JMenu viewMenu;
  private JMenuItem saveMenuItem;
  private JMenuItem loadMenuItem;
  private JMenuItem mapMenuItem;
  private JMenuItem tableMenuItem;
  private JMenuItem hostMenuItem;
  private PopulateMeets pop;

  public MainView(PopulateMeets popTemp) {  
    this.pop = popTemp;

    // Adds a menu bar to the application.
    createMenu();
    setJMenuBar(menuBar);

    // Determines the resolution of the respective PC the program is running on.
    determineSize();
    largeX = width - 600;
    largeY = height - 150;
    smallX = width - largeX;
    smallY = height - 150;

    // initializes the tableView
    tableView = new TableView(pop);
    add(tableView);
    tableView.setLocation(25, 50);
    tableView.setSize(width - 50, height - 150);

    // Initializes the MapView
    mapView = new GeocoderMap(pop);
    add(mapView);
    mapView.setSize(largeX - 50, largeY);
    mapView.setVisible(false);
    mapView.setLocation(25, 50);
    mapView.setBorder(BorderFactory.createLineBorder(Color.BLACK));

    // initializes the mapoptions panel
    mapOptView = new MapOptionsView(pop);
    add(mapOptView);
    mapOptView.setSize(smallX, smallY);
    mapOptView.setVisible(false);
    mapOptView.setLocation(largeX, 50);
    mapOptView.setBorder(BorderFactory
        .createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Map Viewing Options"));
    ((TitledBorder) mapOptView.getBorder()).setTitleFont(new Font("TimesRoman", Font.PLAIN, 18));

    // Initializes the Host View on the JFrame
    hp = new HostInteractionView(pop);
    add(hp);
    hp.setLocation(25, 50);
    hp.setSize(width - 50, height - 150);
    hp.setVisible(false);

    // Initializes the buttons that allow the user to switch views.
    initTabs();



    setTitle("IHSAA Tournament Generator");
    setLayout(null);


    add(tabPanel);
    tabPanel.setLocation(0, (height - 100));

    listener = new Listener(this);
    pack();
    setSize(width, height);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setLocationRelativeTo(null);
    setResizable(false);
    setVisible(true);
    // getContentPane().setBackground(Color.YELLOW);
  }

  public void determineSize() { 
    // (Width x Height)
    gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
    width = gd.getDisplayMode().getWidth();
    height = gd.getDisplayMode().getHeight() - 50;

  }


  public void buildTableView() { // Makes the table visible or invisible
    tableView.setVisible(true);
    mapView.setVisible(false);
    mapOptView.setVisible(false);
    hp.setVisible(false);
    validate();
  }

  public void buildMapView() { // Makes Map functions the view for the user{
    tableView.setVisible(false);
    mapView.setVisible(true);
    mapOptView.setVisible(true);
    hp.setVisible(false);
    validate();
  }

  public void buildHostPanelView() {// Makes List functions the view for the user
    tableView.setVisible(false);
    mapView.setVisible(false);
    mapOptView.setVisible(false);
    hp.setVisible(true);
    validate();
  }

  private void initTabs() {
    this.tabPanel = new JPanel();
    this.tabPanel.setSize(300, 100);
    this.tabs = new JButton[3];

    for (int i = 0; i < tabs.length; i++) {
      if (i == 0) {
        tabs[i] = new JButton("Table View");
      } else if (i == 1) {
        tabs[i] = new JButton("Map View");
      } else if (i == 2) {
        tabs[i] = new JButton("Host View");
      }
      this.tabs[i].setSize(100, 100);
      this.tabPanel.add(tabs[i]);
    }
  }

  private void createMenu() {
    // Create the menu bar.
    menuBar = new JMenuBar();

    fileMenu = new JMenu("File");
    fileMenu.setFont(new Font("Times New Roman", Font.PLAIN, 16));

    viewMenu = new JMenu("View");
    viewMenu.setFont(new Font("Times New Roman", Font.PLAIN, 16));

    menuBar.add(fileMenu);
    menuBar.add(viewMenu);


    // Adds the save menuItem
    saveMenuItem = new JMenuItem("Save Tournament");
    saveMenuItem.setFont(new Font("Times New Roman", Font.PLAIN, 16));
    fileMenu.add(saveMenuItem);

    // Adds the load menuItem
    loadMenuItem = new JMenuItem("Load Tournament");
    loadMenuItem.setFont(new Font("Times New Roman", Font.PLAIN, 16));
    fileMenu.add(loadMenuItem);

    // Adds the TableView item
    tableMenuItem = new JMenuItem("Table View");
    tableMenuItem.setFont(new Font("Times New Roman", Font.PLAIN, 16));
    viewMenu.add(tableMenuItem);

    // adds the HostView item
    hostMenuItem = new JMenuItem("Host View");
    hostMenuItem.setFont(new Font("Times New Roman", Font.PLAIN, 16));
    viewMenu.add(hostMenuItem);

    // Adds the MapView item
    mapMenuItem = new JMenuItem("Map View");
    mapMenuItem.setFont(new Font("Times New Roman", Font.PLAIN, 16));
    viewMenu.add(mapMenuItem);

  }

  public void popUpdateHosts(String oldHost, String newHost, String meetTitle) {
    
    int temp = 0; // needed for parses the corredt sectional, regional, semistate,or state meets.
    //Updates local sectional
    if (meetTitle.contains("Sectional")) {
      for(int i = 0; i < pop.getSectionalNum(); i++) {
        if(pop.getSectionals(i).getTitle().equals(meetTitle)) {
          temp = pop.getSectionals(i).getId() - 1;
          System.out.println("Previous Host: " + pop.getSectionals(temp).getHost());
          break;
        }
      }
      pop.getSectionals(temp).setHost(newHost);
      for (int i = 0; i < pop.getSectionals(temp).getParticipants().size(); i++) {
        if (pop.getSectionals(temp).getParticipants().get(i).getName().equals(oldHost)) {
          pop.getSectionals(temp).getParticipants().get(i).setHost(false);
        }
        if (pop.getSectionals(temp).getParticipants().get(i).getName().equals(newHost)) {
          pop.getSectionals(temp).getParticipants().get(i).setHost(true);
        }
      }
    } else if (meetTitle.contains("Regional")) {//updates local regional
      for(int i = 0; i < pop.getRegionalNum(); i++) {
        if(pop.getRegionals(i).getTitle().equals(meetTitle)) {
          temp = pop.getRegionals(i).getId() - 1;
          System.out.println("Previous Host: " + pop.getSectionals(temp).getHost());
          break;
        }
      }
      pop.getRegionals(temp).setHost(newHost);
      for (int i = 0; i < pop.getRegionals(temp).getParticipants().size(); i++) {
        if (pop.getRegionals(temp).getParticipants().get(i).getName().equals(oldHost)) {
          pop.getRegionals(temp).getParticipants().get(i).setHost(false);
        }
        if (pop.getRegionals(temp).getParticipants().get(i).getName().equals(newHost)) {
          pop.getRegionals(temp).getParticipants().get(i).setHost(true);
        }
      }
    } else if (meetTitle.contains("Semi-State")) {
      for(int i = 0; i < pop.getSemistateNum(); i++) {
        if(pop.getSemistates(i).getTitle().equals(meetTitle)) {
          temp = pop.getSemistates(i).getId() - 1;
          System.out.println("Previous Host: " + pop.getSectionals(temp).getHost());
          break;
        }
      }
      pop.getSemistates(temp).setHost(newHost);
      for (int i = 0; i < pop.getSemistates(temp).getParticipants().size(); i++) {
        if (pop.getSemistates(temp).getParticipants().get(i).getName().equals(oldHost)) {
          pop.getSemistates(temp).getParticipants().get(i).setHost(false);
        }
        if (pop.getSemistates(temp).getParticipants().get(i).getName().equals(newHost)) {
          pop.getSemistates(temp).getParticipants().get(i).setHost(true);
        }
      }
    } else if (meetTitle.contains("State")) {
      for(int i = 0; i < pop.getStateNum(); i++) {
        if(pop.getStates(i).getTitle().equals(meetTitle)) {
          temp = pop.getStates(i).getId() - 1;
          System.out.println("Previous Host: " + pop.getSectionals(temp).getHost());
          break;
        }
      }
      pop.getStates(temp).setHost(newHost);
      for (int i = 0; i < pop.getStates(temp - 1).getParticipants().size(); i++) {
        if (pop.getStates(temp).getParticipants().get(i).getName().equals(oldHost)) {
          pop.getStates(temp).getParticipants().get(i).setHost(false);
        }
        if (pop.getStates(temp).getParticipants().get(i).getName().equals(newHost)) {
          pop.getStates(temp).getParticipants().get(i).setHost(true);
        }
      }
    }
    
    
    //Calls an update method that will give the pop information to all gui components to update.
    //this.hp.updatePop(pop);
    this.mapOptView.updatePop(pop);
    this.tableView.updatePop(pop);
    System.out.println(pop.getSectionals(temp).getTitle());
    System.out.println("Current Host: " + pop.getSectionals(temp).getHost());
    System.out.println("Old Host: " + oldHost);
    System.out.println("New Host: " + newHost);
    System.out.println();
  }

  public TableView getTableView() {
    return this.tableView;
  }

  public JButton[] getTabs() {
    return this.tabs;
  }

  public MapOptionsView getMapOptView() {
    return this.mapOptView;
  }

  public GeocoderMap getMap() {
    return this.mapView;
  }

  public JMenuItem getSaveMenuItem() {
    return saveMenuItem;
  }

  public JMenuItem getLoadMenuItem() {
    return loadMenuItem;
  }

  public JMenuItem getMapMenuItem() {
    return mapMenuItem;
  }

  public JMenuItem getTableMenuItem() {
    return tableMenuItem;
  }

  public JMenuItem getHostMenuItem() {
    return hostMenuItem;
  }

  public PopulateMeets getPop() {
    return this.pop;
  }
  
  public HostInteractionView getHp() {
    return this.hp;
  }

}
