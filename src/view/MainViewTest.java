package view;

import static org.junit.jupiter.api.Assertions.*;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import org.junit.jupiter.api.Test;
import controller.Listener;
import model.database.PopulateMeets;
import view.hostcomponents.HostSwapTab;
import view.mapcomponents.GeocoderMap;
import view.mapcomponents.MapOptionsView;
import view.tablecomponents.TableView;

class MainViewTest {
  
  private JPanel tabPanel;
  private JButton[] tabs;
  private Listener listener;
  private TableView tableView;
  private GraphicsDevice gd;
  private int width;
  private int height;
  private GeocoderMap mapView;
  private MapOptionsView mapOptView;
  private HostSwapTab hp;
  // Persistant positioning of the large and small winows that will occupy the frame.
  private int largeX;
  private int largeY;
  private int smallX;
  private int smallY;
  private JMenuBar menuBar;
  private JMenu fileMenu;
  private JMenu viewMenu;
  private JMenuItem saveMenuItem;
  private JMenuItem loadMenuItem;
  private JMenuItem mapMenuItem;
  private JMenuItem tableMenuItem;
  private JMenuItem hostMenuItem;
  private PopulateMeets pop;
  
  MainView mv = new MainView(pop);
  

  @Test
  void testDetermineSize() {
    gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
    width = gd.getDisplayMode().getWidth();
    height = gd.getDisplayMode().getHeight() - 50;
    
  //  assertNull(gd);
    assertNotNull(width);
   // assertNull(height);
  }

  @Test
  void testBuildTableView() {
    fail("Not yet implemented");
  }

  @Test
  void testBuildMapView() {
    fail("Not yet implemented");
  }

  @Test
  void testBuildHostPanelView() {
    fail("Not yet implemented");
  }

  @Test
  void testGetTableView() {
    fail("Not yet implemented");
  }

  @Test
  void testGetTabs() {
    fail("Not yet implemented");
  }

  @Test
  void testGetMapOptView() {
    fail("Not yet implemented");
  }

  @Test
  void testGetMap() {
    fail("Not yet implemented");
  }

  @Test
  void testGetSaveMenuItem() {
    fail("Not yet implemented");
  }

  @Test
  void testGetLoadMenuItem() {
    fail("Not yet implemented");
  }

  @Test
  void testGetMapMenuItem() {
    fail("Not yet implemented");
  }

  @Test
  void testGetTableMenuItem() {
    fail("Not yet implemented");
  }

  @Test
  void testGetHostMenuItem() {
    fail("Not yet implemented");
  }

  @Test
  void testGetPop() {
    fail("Not yet implemented");
  }

  @Test
  void testGetHostPanel() {
    fail("Not yet implemented");
  }

}
