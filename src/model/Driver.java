package model;

import model.database.PopulateMeets;
import view.MainView;

public class Driver {

	public static void main(String[] args) {

		// Starts the View Package
		long startTime = System.nanoTime();
		PopulateMeets pop = new PopulateMeets();
		long endTime = System.nanoTime();
		long result = (endTime - startTime) / 1000000000;
		System.out.println("Time for Entire Database Retrieval. " + result + " Seconds");
		System.out.println();
		MainView f = new MainView(pop);
	}

}
