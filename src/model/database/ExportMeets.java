package model.database;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.StringTokenizer;
import javax.swing.JOptionPane;
import model.meets.School;

public class ExportMeets {
  private PrintWriter pw;
  private StringBuilder sb;
  private PopulateMeets pop;
  private final String[] colNames = {"School Name", "Address", "Latitude", "Longitude", "Competition Level", 
      "Distance to Host", "Host Status", "Meet Value"};
  private int temp;
  private School tempSchool;
  private StringTokenizer tokenizer;
  private final String delims = ",";
  private String tempStore;
  private String tempAdd;
  /*
   *Host school = true, Not Host = False.
   */
  public ExportMeets(PopulateMeets pop) {
    this.pop = pop;
  }

  public void export() {
    String s = (String)JOptionPane.showInputDialog("What would you like to save this tournament as?");
    if(s == null) {
      return;
    }
    try {
      pw = new PrintWriter(new File(s + ".csv"));
    } catch (FileNotFoundException e) {

      System.out.println("Failure to Export: Error 101e");
    }
    
    //Creates the column headers in the csv
    sb = new StringBuilder(); 
    for(int i = 0; i < colNames.length; i++) {
      sb.append(colNames[i]);
      sb.append(",");
    }
    sb.append("\n");
    
    //Adds all the sectionals to the list
    for(int i =0; i < pop.getSectionals().size(); i++) {
      temp = pop.getSectionals(i).getParticipants().size();
      for(int a = 0; a < temp; a++) {
          tempSchool = pop.getSectionals(i).getParticipants().get(a);
          sb.append(tempSchool.getName());
          sb.append(",");
          tempAdd = "";
          tempStore = tempSchool.getAddress(); // Stores the string to be tokenized
          tokenizer = new StringTokenizer(tempStore, delims);
          while(tokenizer.hasMoreTokens()) {
            tempAdd += tokenizer.nextToken() + " ";
          }
          sb.append(tempAdd);
          sb.append(",");
          
          sb.append(tempSchool.getLatitude().toString());
          sb.append(",");
          
          sb.append(tempSchool.getLongitude().toString());
          sb.append(",");
          
          sb.append(tempSchool.getCompLevel());
          sb.append(",");
          
          sb.append(tempSchool.getDistanceToHost());
          sb.append(",");
          
          sb.append(tempSchool.isHost());
          sb.append(",");
          
          sb.append("S" + (i+1));
          sb.append("\n");
      }
    }
    sb.append("\n");
    
    //Add all regionals to the list
    for(int i =0; i < pop.getRegionals().size(); i++) {
      temp = pop.getRegionals(i).getParticipants().size();
      for(int a = 0; a < temp; a++) {
          tempSchool = pop.getRegionals(i).getParticipants().get(a);
          sb.append(tempSchool.getName());
          sb.append(",");
          tempAdd = "";
          tempStore = tempSchool.getAddress(); // Stores the string to be tokenized
          tokenizer = new StringTokenizer(tempStore, delims);
          while(tokenizer.hasMoreTokens()) {
            tempAdd += tokenizer.nextToken() + " ";
          }
          sb.append(tempAdd);
          sb.append(",");
          
          sb.append(tempSchool.getLatitude().toString());
          sb.append(",");
          
          sb.append(tempSchool.getLongitude().toString());
          sb.append(",");
          
          sb.append(tempSchool.getCompLevel());
          sb.append(",");
          
          sb.append(tempSchool.getDistanceToHost());
          sb.append(",");
          
          sb.append(tempSchool.isHost());
          sb.append(",");
          
          sb.append("R" + (i+1));
          sb.append("\n");
      }
    }
    sb.append("\n");
    
    //add all the semistates to the list
    for(int i =0; i < pop.getSemistates().size(); i++) {
      temp = pop.getSemistates(i).getParticipants().size();
      for(int a = 0; a < temp; a++) {
          tempSchool = pop.getSemistates(i).getParticipants().get(a);
          sb.append(tempSchool.getName());
          sb.append(",");
          tempAdd = "";
          tempStore = tempSchool.getAddress(); // Stores the string to be tokenized
          tokenizer = new StringTokenizer(tempStore, delims);
          while(tokenizer.hasMoreTokens()) {
            tempAdd += tokenizer.nextToken() + " ";
          }
          sb.append(tempAdd);
          sb.append(",");
          
          sb.append(tempSchool.getLatitude().toString());
          sb.append(",");
          
          sb.append(tempSchool.getLongitude().toString());
          sb.append(",");
          
          sb.append(tempSchool.getCompLevel());
          sb.append(",");
          
          sb.append(tempSchool.getDistanceToHost());
          sb.append(",");
          
          sb.append(tempSchool.isHost());
          sb.append(",");
          
          sb.append("SS" + (i+1));
          sb.append("\n");
      }
    }
    sb.append("\n");
    
    //adds all states to the list
    for(int i =0; i < pop.getStates().size(); i++) {
      temp = pop.getStates(i).getParticipants().size();
      for(int a = 0; a < temp; a++) {
          tempSchool = pop.getStates(i).getParticipants().get(a);
          sb.append(tempSchool.getName());
          sb.append(",");
          tempAdd = "";
          tempStore = tempSchool.getAddress(); // Stores the string to be tokenized
          tokenizer = new StringTokenizer(tempStore, delims);
          while(tokenizer.hasMoreTokens()) {
            tempAdd += tokenizer.nextToken() + " ";
          }
          sb.append(tempAdd);
          sb.append(",");
          
          sb.append(tempSchool.getLatitude().toString());
          sb.append(",");
          
          sb.append(tempSchool.getLongitude().toString());
          sb.append(",");
          
          sb.append(tempSchool.getCompLevel());
          sb.append(",");
          
          sb.append(tempSchool.getDistanceToHost());
          sb.append(",");
          
          sb.append(tempSchool.isHost());
          sb.append(",");
          
          sb.append("ST" + (i+1));
          sb.append("\n");
      }
    }
    pw.write(sb.toString());
    pw.close();
    System.out.println("done!");
  }
}
