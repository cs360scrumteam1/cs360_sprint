package model.database;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class MeetData {
	// AWS informtaion
	private final String DB_NAME = "School"; // schema name
	private final String DB_USERNAME = "team9";
	private final String DB_PASSWORD = "1234567890";
	private final String DB_HOSTNAME = "sprintinstance.cvdjhxgkq530.us-east-2.rds.amazonaws.com"; // Endpoint
	private final String DB_PORT = "3306";
	private final String driverName = "com.mysql.jdbc.Driver";
	private final String jdbcUrl = "jdbc:mysql://" + DB_HOSTNAME + ":" + DB_PORT + "/" + DB_NAME + "?user="
			+ DB_USERNAME + "&password=" + DB_PASSWORD;
	
	private Connection conn;
	private PreparedStatement preparedStatement;
	private Statement stat;
	private ResultSet rset;
	private ArrayList<String> data;
	private int num;
	private String temp;
	private float tempAverage;
	private float tempMax;
	private BigDecimal tempLatitude;
	private BigDecimal tempLongtitude;

	public MeetData() {
		try {
			Class.forName(driverName); // JDBC LOAD
		} catch (ClassNotFoundException e) {
			System.out.println(e.getStackTrace());
		}
	}

	public void closeDatabase() {
		try {
			if (conn != null) {
				conn.close();
			}
			if (stat != null) {
				stat.close();
			}
			if (rset != null) {
				rset.close();
			}
		} catch (SQLException e) {
			System.out.println(e.getStackTrace());
		}

	}

	// the last number of section
	public int numSectionals() {
		num = 0;
		try {
			String query = "select * from repository order by sectiontype desc limit 1";

			conn = DriverManager.getConnection(jdbcUrl);
			preparedStatement = conn.prepareStatement(query);
			rset = preparedStatement.executeQuery(query);

			while (rset.next()) {
				temp = rset.getString("sectiontype");
				num = Integer.parseInt(temp);

			}

		} catch (SQLException e) {
			System.out.println("query failed 1" + e.getStackTrace());
		} finally {
			closeDatabase();
		}
		return num;
	}

	// the last number of region
	public int numRegionals() {
		num = 0;
		try {
			String query = "select * from repository order by regiontype desc limit 1";

			conn = DriverManager.getConnection(jdbcUrl);
			preparedStatement = conn.prepareStatement(query);
			rset = preparedStatement.executeQuery(query);

			while (rset.next()) {
				temp = rset.getString("regiontype");
				num = Integer.parseInt(temp);

			}

		} catch (SQLException e) {

			System.out.println("query failed 2" + e.getStackTrace());
		} finally {
			closeDatabase();
		}
		return num;
	}

	// the last number of semi-state
	public int numSemiStates() {
		num = 0;
		try {
			String query = "select * from repository order by semistatetype desc limit 1";

			conn = DriverManager.getConnection(jdbcUrl);
			preparedStatement = conn.prepareStatement(query);
			rset = preparedStatement.executeQuery(query);

			while (rset.next()) {
				temp = rset.getString("semistatetype");
				num = Integer.parseInt(temp);

			}

		} catch (SQLException e) {
			System.out.println("query failed 3" + e.getStackTrace());
		} finally {
			closeDatabase();
		}
		return num;
	}

	// the last number of state
	public int numStates() {
		num = 0;
		try {
			String query = "select * from repository order by statetype desc limit 1";

			conn = DriverManager.getConnection(jdbcUrl);
			preparedStatement = conn.prepareStatement(query);
			rset = preparedStatement.executeQuery(query);

			while (rset.next()) {
				temp = rset.getString("statetype");
				num = Integer.parseInt(temp);
			}

		} catch (SQLException e) {
			System.out.println("query failed 4" + e.getStackTrace());
		} finally {
			closeDatabase();
		}
		return num;
	}

	public String loadSectionalHosts(int sectiontype) {
		temp = "";
		try {
			String query = "select schoolname from repository where sectionhost=1 and sectiontype=" + sectiontype;
			conn = DriverManager.getConnection(jdbcUrl);
			preparedStatement = conn.prepareStatement(query);
			rset = preparedStatement.executeQuery(query);

			while (rset.next()) {
				temp = (rset.getString("schoolname"));
			}
		} catch (SQLException e) {
			System.out.println("query failed 5" + e.getStackTrace());
		} finally {
			closeDatabase();
		}
		return temp;
	}

	public String loadRegionalHosts(int regiontype) {
		temp = "";
		try {
			String query = "select schoolname from repository where regionhost=1 and regiontype=" + regiontype;
			conn = DriverManager.getConnection(jdbcUrl);
			preparedStatement = conn.prepareStatement(query);
			rset = preparedStatement.executeQuery(query);

			while (rset.next()) {
				temp = (rset.getString("schoolname"));
			}
		} catch (SQLException e) {
			System.out.println("query failed 6" + e.getStackTrace());
		} finally {
			closeDatabase();
		}
		return temp;
	}

	// retrieve host school name
	public String loadSemiStateHosts(int semistatetype) {
		temp = "";
		try {
			String query = "select schoolname from repository where semistatehost=1 and semistatetype=" + semistatetype;
			conn = DriverManager.getConnection(jdbcUrl);
			preparedStatement = conn.prepareStatement(query);
			rset = preparedStatement.executeQuery(query);

			while (rset.next()) {
				temp = (rset.getString("schoolname"));
			}
		} catch (SQLException e) {
			System.out.println("query failed 7" + e.getStackTrace());
		} finally {
			closeDatabase();
		}
		return temp;
	}

	// LoadSectionData retrieve schoolname, address, competitionlevel at once
	// dependent on sectional number.

	public ArrayList<String> loadSectionalData(int sectiontype) {
		temp = "";
		data = new ArrayList<String>();
		try {

			String query = "SELECT schoolname, address, competitionlevel, sectiondistance, Latitude, Longtitude from repository where sectiontype ="
					+ sectiontype;

			conn = DriverManager.getConnection(jdbcUrl);
			preparedStatement = conn.prepareStatement(query);
			rset = preparedStatement.executeQuery(query);

			while (rset.next()) {


				temp = rset.getString("schoolname") + ";" + rset.getObject("address") + ";"
                    + rset.getString("competitionlevel")+";"+rset.getString("sectiondistance") + ";"
				    + rset.getBigDecimal("Latitude") + ";" +rset.getBigDecimal("Longtitude") + ";" ;

				data.add(temp);

			}
		}

		catch (SQLException e) {
			System.out.println("query failed 8" + e.getStackTrace());
		} finally {
			closeDatabase();
		}
		return data;
	}

	public ArrayList<String> loadRegionalData(int regiontype) {
		temp = "";
		data = new ArrayList<String>();
		try {

			String query = "SELECT schoolname, address, competitionlevel, regiondistance, Latitude, Longtitude from repository where regiontype ="
					+ regiontype;

			conn = DriverManager.getConnection(jdbcUrl);
			stat = conn.createStatement();
			rset = stat.executeQuery(query);

			while (rset.next()) {


				temp = rset.getString("schoolname") + ";" + rset.getObject("address") + ";"
	                + rset.getString("competitionlevel")+";"+rset.getString("regiondistance") + ";"
	                + rset.getBigDecimal("Latitude") + ";" +rset.getBigDecimal("Longtitude") + ";" ;


				data.add(temp);

			}
		}

		catch (SQLException e) {
			System.out.println("query failed 9" + e.getStackTrace());
		} finally {
			closeDatabase();
		}
		return data;
	}

	public ArrayList<String> loadSemiStateData(int semistatetype) {
		temp = "";
		data = new ArrayList<String>();
		try {

			String query = "SELECT schoolname, address, competitionlevel, semistatedistance, Latitude, Longtitude from repository where semistatetype ="
					+ semistatetype;

			conn = DriverManager.getConnection(jdbcUrl);
			stat = conn.createStatement();
			rset = stat.executeQuery(query);

			while (rset.next()) {


				temp = rset.getString("schoolname") + ";" + rset.getObject("address") + ";"
				+ rset.getString("competitionlevel")+";"+rset.getString("semistatedistance") + ";"
			    + rset.getBigDecimal("Latitude") + ";" +rset.getBigDecimal("Longtitude") + ";" ;

				data.add(temp);
			}
		}

		catch (SQLException e) {
			System.out.println("query failed 10" + e.getStackTrace());
		} finally {
			closeDatabase();
		}
		return data;
	}	

	public ArrayList<String> loadStateData(int statetype) {
		temp = "";
		data = new ArrayList<String>();
		try {
			String query = "SELECT schoolname, address, competitionlevel, Latitude, Longtitude From repository where statetype =" + statetype;

			conn = DriverManager.getConnection(jdbcUrl);
			stat = conn.createStatement();
			rset = stat.executeQuery(query);

			while (rset.next()) {

				temp = rset.getString("schoolname") + ";" + rset.getObject("address") + ";"
						+ rset.getString("competitionlevel") + ";"
						+ rset.getBigDecimal("Latitude") + ";" +rset.getBigDecimal("Longtitude") + ";" ;
				data.add(temp);
			}
		}

		catch (SQLException e) {
			System.out.println("query failed 11" + e.getStackTrace());
		} finally {
			closeDatabase();
		}
		return data;
	}

	
	//Retrieve the average distance from sectional distance - it shows until the one decimal point like 12.5
	//This depends on the sectiontype, 
	//For example, when you put the 5 in the parameter, it retrieve the average distance among the section 5 distances.
	
	public float loadSectionalAverage(int sectiontype) {
	  tempAverage = 0;
		try {
			String query = "SELECT Round(avg(sectiondistance),1) AS Avgdistance From repository where sectiontype =" + sectiontype;

			conn = DriverManager.getConnection(jdbcUrl);
			stat = conn.createStatement();
			rset = stat.executeQuery(query);

			while (rset.next()) {
			  tempAverage = Float.parseFloat(rset.getString("Avgdistance"));
			}
		}

		catch (SQLException e) {
			System.out.println("query failed 13" + e.getStackTrace());
		} finally {
			closeDatabase();
		}
		return tempAverage;
	}
	
	public float loadRegionalAverage(int regiontype) {
		tempAverage = 0;
		try {
			String query = "SELECT Round(avg(regiondistance),1) AS Avgdistance From repository where regiontype =" + regiontype;

			conn = DriverManager.getConnection(jdbcUrl);
			stat = conn.createStatement();
			rset = stat.executeQuery(query);

			while (rset.next()) {

				tempAverage = Float.parseFloat(rset.getString("Avgdistance"));
			}
		}

		catch (SQLException e) {
			System.out.println("query failed 14" + e.getStackTrace());
		} finally {
			closeDatabase();
		}
		return tempAverage;
	}
	
	public float loadSemiStateAverage(int semistatetype) {
		tempAverage = 0;
		try {
			String query = "SELECT Round(avg(semistatedistance),1) AS Avgdistance From repository where semistatetype =" + semistatetype;

			conn = DriverManager.getConnection(jdbcUrl);
			stat = conn.createStatement();
			rset = stat.executeQuery(query);

			while (rset.next()) {
			  tempAverage = Float.parseFloat(rset.getString("Avgdistance"));
			}
		}

		catch (SQLException e) {
			System.out.println("query failed 15" + e.getStackTrace());
		} finally {
			closeDatabase();
		}
		return tempAverage;
	}
	
	//Retrieve the farthest distance from sectional distance
	//This depends on the sectiontype, 
	//For example, when you put the 5 in the parameter, it retrieve the farthest distance among the section 5 distances.
	
	public float loadSectionalMax(int sectiontype) {
		tempMax = 0;
		try {
			String query = "SELECT Round(max(sectiondistance),1) AS Maxdistance From repository where sectiontype = " + sectiontype;

			conn = DriverManager.getConnection(jdbcUrl);
			stat = conn.createStatement();
			rset = stat.executeQuery(query);
			while (rset.next()) {
				tempMax = Float.parseFloat(rset.getString("Maxdistance"));
			}
		} catch (SQLException e) {
			System.out.println("query failed 16" + e.getStackTrace());
		} finally {
			closeDatabase();
		}
		return tempMax;
	}
	
	public float loadRegionalMax(int regiontype) {
		tempMax = 0;
		try {
			String query = "SELECT Round(max(regiondistance),1) AS Maxdistance From repository where regiontype = " + regiontype;

			conn = DriverManager.getConnection(jdbcUrl);
			stat = conn.createStatement();
			rset = stat.executeQuery(query);

			while (rset.next()) {
              tempMax = Float.parseFloat(rset.getString("Maxdistance"));
			}
		} catch (SQLException e) {
			System.out.println("query failed 17" + e.getStackTrace());
		} finally {
			closeDatabase();
		}
		return tempMax;
	}
	
	public float loadSemiStateMax(int semistatetype) {
		tempMax =0;
		try {
			String query = "SELECT Round(max(semistatedistance),1) AS Maxdistance From repository where semistatetype = " + semistatetype;

			conn = DriverManager.getConnection(jdbcUrl);
			stat = conn.createStatement();
			rset = stat.executeQuery(query);

			while (rset.next()) {
              tempMax = Float.parseFloat(rset.getString("Maxdistance"));
			}
		} catch (SQLException e) {
			System.out.println("query failed 18" + e.getStackTrace());
		} finally {
			closeDatabase();
		}
		return tempMax;
	}
	
	   public BigDecimal LoadLatitude(int sectiontype) {
	     tempLatitude = new BigDecimal(0.1);
	        try {
	            String query = "SELECT Latitude From repository where sectiontype = " + sectiontype;

	            conn = DriverManager.getConnection(jdbcUrl);
	            stat = conn.createStatement();
	            rset = stat.executeQuery(query);

	            while (rset.next()) {
	              tempAverage = Float.parseFloat(rset.getString("Latitude"));
	            }
	        } catch (SQLException e) {
	            System.out.println("query failed 19" + e.getStackTrace());
	        } finally {
	            closeDatabase();
	        }
	        return tempLatitude;
	    }
	   
       public BigDecimal LoadLongtitude(int sectiontype) {
         tempLongtitude = new BigDecimal(0.1);
            try {
                String query = "SELECT Longtitude From repository where sectiontype" + sectiontype;

                conn = DriverManager.getConnection(jdbcUrl);
                stat = conn.createStatement();
                rset = stat.executeQuery(query);

                while (rset.next()) {
                  tempAverage = Float.parseFloat(rset.getString("Longtitude"));
                }
            } catch (SQLException e) {
                System.out.println("query failed 20" + e.getStackTrace());
            } finally {
                closeDatabase();
            }
            return tempLongtitude;
        }
}