package model.meets;

import java.util.ArrayList;

public class Meet {
  private String title;
  private int id;
  private float averageDistance;
  private float farthestDistance;
  private ArrayList<School> participants;
  private String host;

  public Meet() {
    title = "";
    id = 0;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public ArrayList<School> getParticipants() {
    return participants;
  }

  public void setParticipants(ArrayList<School> participants) {
    this.participants = participants;
  }



  public float getAverageDistance() {
    return averageDistance;
  }

  public void setAverageDistance(float tempAverageDistance) {
    this.averageDistance = tempAverageDistance;
  }

  public float getFarthestDistance() {
    return farthestDistance;
  }

  public void setFarthestDistance(float farthestDistance) {
    this.farthestDistance = farthestDistance;
  }

  public String toString() {
    String message = this.getTitle();
    return message;
  }

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }
  
}
