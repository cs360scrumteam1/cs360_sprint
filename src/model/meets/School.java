package model.meets;

import java.math.BigDecimal;

public class School {
  private String name; // String Name of the School
  private String address; // String address of the school
  private boolean host; // Host school = true, Not Host = False.
  private String compLevel; // Level of competition
  private float distanceToHost;
  private BigDecimal latitude;
  private BigDecimal longitude;

  public School(String name, String address, boolean host, String compLevel, float distanceToHost,
      BigDecimal latitude, BigDecimal longitude) {
    this.name = name;
    this.address = address;
    this.host = host;
    this.compLevel = compLevel;
    this.distanceToHost = distanceToHost;
    this.latitude = latitude;
    this.longitude = longitude;

  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public boolean isHost() {
    return host;
  }

  public void setHost(boolean host) {
    this.host = host;
  }

  public String getCompLevel() {
    return compLevel;
  }

  public void setCompLevel(String compLevel) {
    this.compLevel = compLevel;
  }

  public float getDistanceToHost() {
    return distanceToHost;
  }

  public void setDistanceToHost(float distanceToHost) {
    this.distanceToHost = distanceToHost;
  }

  public BigDecimal getLatitude() {
    return latitude;
  }

  public void setLatitude(BigDecimal latitude) {
    this.latitude = latitude;
  }

  public BigDecimal getLongitude() {
    return longitude;
  }

  public void setLongitude(BigDecimal longitude) {
    this.longitude = longitude;
  }

  
}
